import matplotlib
import numpy as np 
import matplotlib.pyplot as plt
import scipy.interpolate as inter
import astropy.constants as con
from sedpy import observate
from scipy import integrate
from scipy.optimize import curve_fit

matplotlib.style.use('classic')

SIM = "TNG50-1"
from data import *

snapnum = 33

outpath = outpath_C+"/"+SIM+"/snap_"+str(snapnum)+"/"

def find_value(l,value):
	return len(l[l<value])

def get_slope(lamb,flux,startloc,endloc):
        #lamb=lamb*1e4 #unit A
        #flux=flux*(1*1000*1000/10.)**2 #unit Jy
        #lum=flux * 1e-23 * 4.* np.pi * (10.*con.pc.value*100)**2 #unit erg/s/Hz

        index = np.arange(0,len(lamb),dtype=np.int32)
        startid = index[(lamb-startloc)>0][0]
        endid   = index[(lamb-endloc)<0][-1]

        UVlamb = lamb[startid:endid+1]
        flamb = flux[startid:endid+1]*con.c.value/lamb[startid:endid+1]**2

        if len(flamb[np.invert(np.isfinite(flamb))]) != 0: return np.nan
        if len(flamb[flamb<=0]) != 0: return np.nan

        def flin(x,beta,A):
                return beta*(x-np.log10(0.1600))+A

        def fpower(x,beta,A):
                return A*np.power(x/0.1600,beta)

        best_fit,_ = curve_fit(flin, np.log10(UVlamb), np.log10(flamb), p0=(-1,np.log10(flamb[0])) )
        return best_fit[0], best_fit[1]

def plot_sed(subid):
	fname = outpath+"output"+str(subid)+"/dusty0_neb_sed.dat"
	data=np.genfromtxt(fname,names=('lamb','fnu'))
	lamb=data['lamb']
	flux=data['fnu']
	lum =flux * 1e-23 * 4 * np.pi * (con.pc.value*100*1e6)**2 / (3.826e33) * (con.c.value * 1e6/lamb) 
	ax.plot(lamb, lum, c='crimson')

	beta,A = get_slope(lamb, flux, 0.1450, 0.3000)
	xfit = np.linspace(0.1000, 0.3500, 100)
	yfit = 10**A * np.power(xfit/0.1600, beta)
	yfit = yfit * 1e-23 * 4 * np.pi * (con.pc.value*100*1e6)**2 / (3.826e33) * (xfit * 10**6)
	ax.plot(xfit, yfit, '--', c="pink" )

	fname = outpath+"output"+str(subid)+"/nodust_neb_sed.dat"
        data=np.genfromtxt(fname,names=('lamb','fnu'))
        lamb=data['lamb']
        flux=data['fnu']
        lum =flux * 1e-23 * 4 * np.pi * (con.pc.value*100*1e6)**2 / (3.826e33) * (con.c.value * 1e6/lamb)
        ax.plot(lamb, lum, c='royalblue')

	beta,A = get_slope(lamb, flux, 0.1450, 0.3000)
        xfit = np.linspace(0.1000, 0.3500, 100)
        yfit = 10**A * np.power(xfit/0.1600, beta)
        yfit = yfit * 1e-23 * 4 * np.pi * (con.pc.value*100*1e6)**2 / (3.826e33) * (xfit * 10**6)
        ax.plot(xfit, yfit, '--', c="cyan" )

matplotlib.rc('xtick.major', size=12, width=4)
matplotlib.rc('xtick.minor', size=6, width=4)
matplotlib.rc('ytick.major', size=12, width=4)
matplotlib.rc('ytick.minor', size=6, width=4)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(1,figsize=(15,10))
rect = [0.11,0.13,0.84,0.79]
ax=plt.axes(rect)

#subid = 44315
#subid = 31832
subid = 0
plot_sed(subid)

ax.axvspan(0.1450, 0.3000, color="gray", alpha=0.2)

plt.xscale('log')
plt.yscale('log')
plt.xlim(0.1000,2.00)
#plt.ylim(2e10,7e12)  #44315
#plt.ylim(5e9,4e11)  #31832
plt.ylim(2e10,7e12)  #0

plt.xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40)
plt.ylabel(r'$\lambda \, f_{\lambda}$ $[{\rm L}_\odot]$',fontsize=40,labelpad=-1.5)
plt.tick_params(direction='in',top='on',right='on',labelsize=25)
plt.tick_params(axis='x', pad=10.0)
plt.tick_params(axis='y', pad=10.0)
#plt.legend(fontsize=15,loc="upper right")
plt.minorticks_on()
plt.tick_params(which='minor',direction='in',top='on',right='on')
plt.tick_params(direction='in',top='on',right='on',labelsize=25)

#plt.xticks([0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.7], ['0.35','0.4','0.45','0.5','0.55','0.6','0.7'])

fig.tight_layout()
plt.savefig('../figs/SED_'+str(subid)+'.pdf',format='pdf')
#plt.show()

