import numpy as np
from scipy.integrate import romberg
from scipy.interpolate import interp1d
import astropy.constants as con
import sys
import os
import tables
SIM=sys.argv[1]
from data import *
snapnum=int(sys.argv[2])
from mpi4py import MPI

#outpath='/n/mvogelsfs01/sxj/DUST_MODEL/modelC/TUNE_backup/'
#outpath=outpath_C_ndust
if snapnum>13: outpath=outpath_C
else: outpath=outpath_C_nmap

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

line_info={
	"Halpha"  :np.array([6563., 6463., 6663., 6163., 6963.]),
	"Hbeta"   :np.array([4861., 4801., 4900., 4561., 4921.]),
	#"OII"     :np.array([3727., 3627., 3827., 3327., 4127.]),
	"OII"     :np.array([3727., 3677., 3757., 3427., 3757.]),
	"OIII"    :np.array([5000., 4921., 5100., 4900., 5300.]),
	"OIII5008":np.array([5008., 4980., 5100., 4980., 5500.])
}

def get_Lline(fname,lineloc):	

        data=np.genfromtxt(fname,names=("lamb","fnu"))
        lamb=data['lamb']*1e-6 #unit m
        flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
        lum=flux * 1e-23 * 4.* np.pi * (10.*con.pc.value*100)**2 #unit erg/s/Hz

        lamb=np.append(np.append(1e-20,lamb),1e10)
        lum=np.append(np.append(0,lum),0)

        map_grid=np.genfromtxt("waves_map.dat")[:,0]
        fall=interp1d(lamb, lum)
        fmap=fall(map_grid)

        frequency = con.c.value/map_grid #unit Hz
        spec = fmap
        f=interp1d(frequency, spec)
        BB=romberg(f , 3e8/lineloc[4]*1e10  , 3e8/lineloc[3]*1e10 ,  divmax=100)
        NB=romberg(f , 3e8/lineloc[2]*1e10  , 3e8/lineloc[1]*1e10 ,  divmax=100)
        wBB=lineloc[4]-lineloc[3]
        wNB=lineloc[2]-lineloc[1]
        fBB=BB/wBB
        fNB=NB/wNB

        if (fBB==0) or (fNB==0):
                EW=0
                Lline=0
        else:
                EW= wNB* (fNB-fBB)/(fBB-fNB*wNB/wBB)
                Lline= wNB* (fNB-fBB)/(1-wNB/wBB)
	if Lline>0:
		return np.log10(Lline), EW
	else: return -np.inf, 0

comm.Barrier()

ids=np.genfromtxt(outpath+SIM+"/snap_"+str(snapnum)+"/subids")

if len(ids.shape)!=0:
	Ntotsubs               = ids.shape[0]
	ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')

	logLs=np.zeros((len(ids),len(line_info.keys()),2))
        EWs=np.zeros((len(ids),len(line_info.keys()),2))	
	logLs_global=np.zeros((len(ids),len(line_info.keys()),2))
        EWs_global=np.zeros((len(ids),len(line_info.keys()),2))

	index                  = (ids % size) == rank
	dosubs                 = ids[index]
	Ndosubs                = dosubs.shape[0]
	doid_in_subgroup       = ids_in_subgroup[index]

	for si in range(0, Ndosubs):	
		print rank, si, Ndosubs, Ntotsubs
        	sys.stdout.flush()
		subnum=dosubs[si]
	        relaid=doid_in_subgroup[si]

		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/nodust_neb_sed.dat'
		if os.path.isfile(fname)==True: 
			for j, line_name in enumerate(line_info.keys()):
				logLs[relaid,j,0],EWs[relaid,j,0]=get_Lline(fname,line_info[line_name])
		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/dusty0_neb_sed.dat'
                if os.path.isfile(fname)==True: 
			for j, line_name in enumerate(line_info.keys()):
				logLs[relaid,j,1],EWs[relaid,j,1]=get_Lline(fname,line_info[line_name])

	comm.Barrier()
	comm.Allreduce(logLs,        logLs_global,     op=MPI.SUM)
	comm.Allreduce(EWs,          EWs_global,       op=MPI.SUM)

if rank==0:
	fname="./outpath/"+SIM+"/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "line_luminosity", logLs_global)
	f.create_array(f.root, "equivalent_width", EWs_global)

	if len(ids.shape)==0: f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
	else: f.create_array(f.root, "subids", ids.astype(np.uint32))
	f.create_array(f.root, "emission_line_names", line_info.keys())
	f.close()

