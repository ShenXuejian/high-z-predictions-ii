import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = " "
from data import *
from line_module import *
import matplotlib.font_manager

from scipy.optimize import curve_fit

matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
        '33' :{'TNG50-1':np.array([[38,43.0],[38,43.0]]),
		'TNG100-1':np.array([[41.6,43.5],[41.6,43.5]]),
		'TNG300-1':np.array([[42.3,45],[42.6,45]])},
	'25' :{'TNG50-1':np.array([[38,43.0],[38,43.4]]),
                'TNG100-1':np.array([[41.4,43.5],[41.6,43.5]]),
                'TNG300-1':np.array([[42.7,45],[43.0,45]])},
        '21' :{'TNG50-1':np.array([[38,43.0],[38,43.4]]),
		'TNG100-1':np.array([[42.0,43.5],[42.0,43.5]]),
		'TNG300-1':np.array([[42.7,45],[43.0,45]])},
	'17' :{'TNG50-1':np.array([[38,43.0],[38,43.4]]),
                'TNG100-1':np.array([[41.6,43.5],[41.6,43.5]]),
                'TNG300-1':np.array([[42.7,45],[43.0,45]])}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum,ieff):
        v50[np.invert(np.isfinite(v50))]=0
        n50[np.invert(np.isfinite(v50))]=1e-37
        v100[np.invert(np.isfinite(v100))]=0
        n100[np.invert(np.isfinite(v100))]=1e-37
        v300[np.invert(np.isfinite(v300))]=0
        n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
        binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][ieff,1])
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][ieff,1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][ieff,1]) #& (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def sch_halpha(L,phi_s,L_s,alpha):
        return np.log10(np.log(10.)*(10**phi_s)*np.power(10**L/10**L_s,alpha+1)*np.exp(-10**L/10**L_s))

#later used to do redshift corrections on observational binned estimations
p1=np.array([-2.61,42.56,-1.62])
p2=np.array([-2.78,42.87,-1.59])
pf= (p2-p1)/(2.23-1.47)*(2-1.47)+p1
print pf

def fit_halpha(logL, logphi):
	select = (logL>40.5) & (logphi>-6)
	args,_ = curve_fit(sch_halpha, logL[select], logphi[select], p0=(-2.7,42.,-1.5))
	return args

bmin=39.
bmax=44.
n_bins=24
bins=np.linspace(bmin,bmax,n_bins)
cx= (bins[1:]+bins[:-1])/2.

def cal_LF(logL,boxlength):
        result1,b=np.histogram(logL,bins=bins)
        n1=result1
        lenbin=b[5]-b[4]
        result1=result1/lenbin/((boxlength/1000./hubble)**3)
        result1=np.log10(result1)
        return result1, n1
	
fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

def plot_snap(snapnum, color, label, obs=True):
        mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()

	fname="outpath/TNG50-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname) 
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	lineid1 = np.array(linenames) == "Halpha"
	lineid1 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid1]) 

	#NII corrections introduced in paper I
	f=-0.296*np.log10(EW[:,lineid1,1])+0.8
        f[f<0]=0
        logL[:,lineid1,0] = logL[:,lineid1,0] + np.log10(1./(1.+f))
        logL[:,lineid1,1] = logL[:,lineid1,1] + np.log10(1./(1.+f))

	logL[np.invert(np.isfinite(logL))]=0

	LF_50_1,N_50_1=cal_LF(logL[:,lineid1,1],35000.)
	#ax.plot(cx,LF_50_1,'-',c='royalblue', label=r'$\rm with$ $\rm all$ $\rm dust$')
	
	LF_50_2,N_50_2=cal_LF(logL[:,lineid1,0],35000.)
	#ax.plot(cx,LF_50_2,'-',c='royalblue', label=r'$\rm without$ $\rm resolved$ $\rm dust$')

	fname="outpath/TNG100-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW  =f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	f.close()
	
	f=-0.296*np.log10(EW[:,lineid1,1])+0.8
        f[f<0]=0
        logL[:,lineid1,0] = logL[:,lineid1,0] + np.log10(1./(1.+f))
        logL[:,lineid1,1] = logL[:,lineid1,1] + np.log10(1./(1.+f))

	#resolution corrections 
	halomass=mhalo['TNG100-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,1))

	logL[np.invert(np.isfinite(logL))]=0

	LF_100_1,N_100_1=cal_LF(logL[:,lineid1,1],75000.)
	#ax.plot(cx,LF_100_1,'-',c='crimson')
	LF_100_2,N_100_2=cal_LF(logL[:,lineid1,0],75000.)
	#ax.plot(cx,LF_100_2,'-',c='crimson')

	fname="outpath/TNG300-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW  =f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	f.close()
	
	f=-0.296*np.log10(EW[:,lineid1,1])+0.8
        f[f<0]=0
        logL[:,lineid1,0] = logL[:,lineid1,0] + np.log10(1./(1.+f))
        logL[:,lineid1,1] = logL[:,lineid1,1] + np.log10(1./(1.+f))

	halomass=mhalo['TNG300-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,1))
	
	logL[np.invert(np.isfinite(logL))]=0
	
	LF_300_1,N_300_1=cal_LF(logL[:,lineid1,1],205000.)
	#ax.plot(cx,LF_300_1,'-',c='seagreen')
	LF_300_2,N_300_2=cal_LF(logL[:,lineid1,0],205000.)
	#ax.plot(cx,LF_300_2,'-',c='seagreen')
	
	LF_combined = combine(LF_50_1,LF_100_1,LF_300_1,N_50_1,N_100_1,N_300_1,snapnum,0)
	#ax.plot(cx,LF_combined,'-',c=color, label=label)
	#ax.plot(cx,LF_combined,'.',c=color, mec=color, marker='o', ms=10)	

	args = fit_halpha(cx, LF_combined)
	print snapnum, " : ", args 

	xfit=np.linspace(38,45,100)
	ax.plot(xfit, sch_halpha(xfit, *args), c=color, label=label)

	#LF_combined = combine(LF_50_2,LF_100_2,LF_300_2,N_50_2,N_100_2,N_300_2,snapnum,1)
	#ax.plot(cx,LF_combined,'--',dashes=(25,15),c=color, alpha=0.5)

	if obs!=True: return 0
	#####
	if snapnum==33:
		# the binned estimations undergo redshift corrections, we also remove the extinction corrections in these works 
		data=np.genfromtxt("obdata/sobral2013.dat",names=True)
		phi_corr=sch_halpha(data["L"],*pf)-sch_halpha(data["L"],*p2)
		ax.errorbar(data["L"]-1./2.5,data["LogPhi"]+phi_corr,yerr=data["err"],c='black',mec='black',linestyle='none',marker='o',markersize=10,capsize=6,capthick=2)

		data=np.genfromtxt("obdata/lee2012_corr.dat",names=True)
		phi_corr= sch_halpha(data["L"],*pf)-sch_halpha(data["L"],*p2)
		ax.errorbar(data["L"]-0.5/2.5,np.log10(data["Phi"])+phi_corr,yerr=np.array([ np.log10(data["Phi"])-np.log10(data["Phi"]-data["err"]), np.log10(data["Phi"]+data["err"])-np.log10(data["Phi"])]),c='k',mec='k',linestyle='none',marker='o',markersize=10,capsize=6,capthick=2,label=r'$\rm L12\,+\,S13$'+'\n' + r'($\rm scaled$ $\rm to$ $\rm z=2$)')

###############################################################

xfit=np.linspace(38,45,100)
plt.plot(xfit-1/2.5, sch_halpha(xfit, -3.12, 41.95, -1.75), '--', dashes=(25,15), c="lightgray", label=r'$\rm S13:$ $\rm z=0.40$')
plt.plot(xfit-1/2.5, sch_halpha(xfit, -2.47, 42.25, -1.56), '--', dashes=(25,15), c="darkgray", label=r'$\rm S13:$ $\rm z=0.84$')
plt.plot(xfit-1/2.5, sch_halpha(xfit, -2.78, 42.87, -1.59), '--', dashes=(25,15), c="dimgray", label=r'$\rm S13:$ $\rm z=2.23$')

colors = ("#fbb4b9","#f768a1","#c51b8a","#7a0177")
plot_snap(33, colors[0], r'$\rm TNG:$ $\rm z=2$', obs=True)
plot_snap(25, colors[1], r'$\rm TNG:$ $\rm z=3$', obs=False)
plot_snap(21, colors[2], r'$\rm TNG:$ $\rm z=4$', obs=False)
plot_snap(17, colors[3], r'$\rm TNG:$ $\rm z=5$', obs=False)

#ax.plot([],[],'k-',label=r'$\rm TNG:$ $\rm with$ $\rm all$ $\rm dust$')
#ax.plot([],[],'k--',dashes=(25,15),label=r'$\rm without$ $\rm resolved$ $\rm dust$')

prop = matplotlib.font_manager.FontProperties(size=27)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=False)
	
#ax.text(0.85, 0.92, r'$\rm z='+str(redshift)+'$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

ax.set_xlim(40.1,43.5+0.2)
ax.set_ylim(-6.35,-0.75)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(L_{{\rm H}_{\alpha}}[{\rm erg}/{\rm s}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\phi[{\rm cMpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'Halpha_lf_z345.pdf',format='pdf')
