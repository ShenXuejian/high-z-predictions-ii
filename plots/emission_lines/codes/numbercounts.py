import matplotlib
import matplotlib.pyplot as plt
import scipy.special as spe
from scipy.optimize import curve_fit as fit
import numpy as np
import tables
import sys
SIM = 'TNG300-1'
from data import *
import matplotlib.font_manager
from astropy.cosmology import FlatLambdaCDM
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

import scipy.special as spe
import mpmath
def cumulative(L_lim, Phis, Ls, alpha ):
	return (10**Phis)*mpmath.gammainc(alpha+1., a=np.power(10.,L_lim-Ls))

fits={
"33"  :  (-2.55745737, 42.3970123 , -1.4485998 ),
"25"  :  (-2.70133741, 42.52820288, -1.54204037),
"8"   :  (-4.11511323, 43.41206175, -1.6046747 )
}

detection_limit=np.array([40.93,41.73])
redshifts = np.array([3,8])

nexp = cumulative(detection_limit[0],*fits["25"])*cosmo.differential_comoving_volume(redshifts[0]).value* 1.* 1. *(1./60./180.*np.pi)**2
print nexp

nexp = cumulative(detection_limit[1],*fits["8"])*cosmo.differential_comoving_volume(redshifts[1]).value*1.* 1. *(1./60./180.*np.pi)**2
print nexp
