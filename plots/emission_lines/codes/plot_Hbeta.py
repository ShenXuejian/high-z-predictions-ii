import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = " "
from data import *
from line_module import *
import matplotlib.font_manager
from astropy.cosmology import FlatLambdaCDM
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

from scipy.optimize import curve_fit

snapnum = int(sys.argv[1])
redshift = int(np.array(Zs)[np.array(Snaps)==snapnum])

matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
        '33' :{'TNG50-1':np.array([[38,43.0],[38,43.0]]),
		'TNG100-1':np.array([[41.6,43.5],[41.6,43.5]]),
		'TNG300-1':np.array([[42.3,45],[42.6,45]])},
	'25' :{'TNG50-1':np.array([[38,43.0],[38,43.4]]),
                'TNG100-1':np.array([[41.6,43.5],[41.6,43.5]]),
                'TNG300-1':np.array([[42.7,45],[43.0,45]])},
        '21' :{'TNG50-1':np.array([[38,43.0],[38,43.4]]),
		'TNG100-1':np.array([[42.0,43.5],[42.0,43.5]]),
		'TNG300-1':np.array([[42.7,45],[43.0,45]])},
	'17' :{'TNG50-1':np.array([[38,43.0],[38,43.4]]),
                'TNG100-1':np.array([[41.65,43.5],[41.65,43.5]]),
                'TNG300-1':np.array([[42.7,45],[43.0,45]])},
	'13' :{'TNG50-1':np.array([[38,43.0],[38,43.4]]),
                'TNG100-1':np.array([[42.0,43.5],[42.0,43.5]]),
                'TNG300-1':np.array([[42.7,45],[43.0,45]])},
	'11' :{'TNG50-1':np.array([[38,43.2],[38,43.4]]),
                'TNG100-1':np.array([[42.8,43.8],[42.8,43.8]]),
                'TNG300-1':np.array([[42.8,45],[43.8,45]])},
	'8'  :{'TNG50-1':np.array([[38,43.2],[38,43.4]]),
                'TNG100-1':np.array([[42.7,43.8],[42.7,43.8]]),
                'TNG300-1':np.array([[43.7,45],[43.9,45]])}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum,ieff):
        v50[np.invert(np.isfinite(v50))]=0
        n50[np.invert(np.isfinite(v50))]=1e-37
        v100[np.invert(np.isfinite(v100))]=0
        n100[np.invert(np.isfinite(v100))]=1e-37
        v300[np.invert(np.isfinite(v300))]=0
        n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
        binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][ieff,1])
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][ieff,1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][ieff,0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][ieff,1]) #& (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def sch_hbeta(L,phi_s,L_s,alpha):
        return np.log10(np.log(10.)*(10**phi_s)*np.power(10**L/10**L_s,alpha+1)*np.exp(-10**L/10**L_s))

def fit_beta(logL, logphi):
        select = (logL>40.5) & (logphi>-6)
        args,_ = curve_fit(sch_hbeta, logL[select], logphi[select], p0=(-2.7,42.,-1.5))
        return args

bmin=40.5
bmax=44.5
n_bins=20
bins=np.linspace(bmin,bmax,n_bins)
cx= (bins[1:]+bins[:-1])/2.

def cal_LF(logL,boxlength):
        result1,b=np.histogram(logL,bins=bins)
        n1=result1
        lenbin=b[5]-b[4]
        result1=result1/lenbin/((boxlength/1000./hubble)**3)
        result1=np.log10(result1)
        return result1, n1

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

def addlum(x,y):
	return np.log10(10**x+10**y)

def plot_snap(snapnum,obs=True):
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
	f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
	mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
	mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
	mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
	f.close()

	fname="outpath/TNG50-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname) 
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	lineid1 = np.array(linenames) == "Hbeta"
	lineid1 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid1]) 
	lineid2 = np.array(linenames) == "OIII2"
	lineid2 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid2])

	logL[np.invert(np.isfinite(logL))]=0

	LF_50_1,N_50_1=cal_LF(addlum(logL[:,lineid1,1],logL[:,lineid2,1]),35000.)
	#ax.plot(cx,LF_50_1,'-',c='royalblue', label=r'$\rm with$ $\rm all$ $\rm dust$')

	LF_50_2,N_50_2=cal_LF(addlum(logL[:,lineid1,0],logL[:,lineid2,0]),35000.)
	#ax.plot(cx,LF_50_2,'-',c='royalblue', label=r'$\rm without$ $\rm resolved$ $\rm dust$')

	fname="outpath/TNG100-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW  =f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	f.close()

	halomass=mhalo['TNG100-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,1))

	logL[:,lineid2,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,0))
	logL[:,lineid2,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,1))

	logL[np.invert(np.isfinite(logL))]=0

	LF_100_1,N_100_1=cal_LF(addlum(logL[:,lineid1,1],logL[:,lineid2,1]),75000.)
	#ax.plot(cx,LF_100_1,'-',c='crimson')
	LF_100_2,N_100_2=cal_LF(addlum(logL[:,lineid1,0],logL[:,lineid2,0]),75000.)
	#ax.plot(cx,LF_100_2,'-',c='crimson')

	fname="outpath/TNG300-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW  =f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	f.close()

	halomass=mhalo['TNG300-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,1))

	logL[:,lineid2,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,0))
	logL[:,lineid2,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,1))

	logL[np.invert(np.isfinite(logL))]=0

	LF_300_1,N_300_1=cal_LF(addlum(logL[:,lineid1,1],logL[:,lineid2,1]),205000.)
	#ax.plot(cx,LF_300_1,'-',c='seagreen')
	LF_300_2,N_300_2=cal_LF(addlum(logL[:,lineid1,0],logL[:,lineid2,0]),205000.)
	#ax.plot(cx,LF_300_2,'-',c='seagreen')

	LF_combined = combine(LF_50_1,LF_100_1,LF_300_1,N_50_1,N_100_1,N_300_1,snapnum,0)
	if snapnum==33: ax.plot(cx,LF_combined,'-',c='crimson',label=r'$\rm TNG:$ $\rm with$ $\rm all$ $\rm dust$')
	elif snapnum==25: ax.plot(cx,LF_combined,'-',c='crimson')

	args = fit_beta(cx, LF_combined)
	print snapnum, " : ", args
        #xfit=np.linspace(38,45,100)
        #if snapnum>13: ax.plot(xfit, sch_hbeta(xfit, *args), c="gray")

	LF_combined = combine(LF_50_2,LF_100_2,LF_300_2,N_50_2,N_100_2,N_300_2,snapnum,1)
	if snapnum==33: ax.plot(cx,LF_combined,'-', c='royalblue',label=r'$\rm TNG:$ $\rm without$ $\rm resolved$ $\rm dust$')
	elif snapnum==25: ax.plot(cx,LF_combined,'-', c='royalblue')

	if obs!=True: return 0
	##### observations
	if snapnum in [33,25]:
		data=np.genfromtxt("obdata/Khostovan15_hbeta.dat",names=True)
		id = data['z']==redshift
		if snapnum==33: ax.errorbar(data["logL"][id],data["logPhi"][id],yerr=(data["logPhi"][id]-data["lo"][id], data['up'][id]-data['logPhi'][id]),c='k',mec='k',linestyle='none',marker='o',markersize=15,capsize=0,label=r'$\rm Khostovan+$ $\rm 2015$')
		else: ax.errorbar(data["logL"][id],data["logPhi"][id],yerr=(data["logPhi"][id]-data["lo"][id], data['up'][id]-data['logPhi'][id]),c='k',mec='k',linestyle='none',marker='o',markersize=15,capsize=0)

	if snapnum==25:
		data=np.genfromtxt("obdata/matthee17_hbeta.dat",names=True)
        	ax.errorbar(data["logL"],data["logPhi"],yerr=(data["logPhi"]-data["lo"], data['up']-data['logPhi']),c='darkorchid',mec='darkorchid',linestyle='none',marker='o',markersize=15,capsize=0,label=r'$\rm Matthee+$ $\rm 2017$')

	if snapnum==25:
		#ax.axhline( np.log10(1./(cosmo.differential_comoving_volume(3.).value*1.* 9.2*(1./60./180.*np.pi)**2)),linestyle='--',lw=5,color='grey',alpha=0.6)
        	#ax.axvline( 29,linestyle='--',dashes=(25,15),lw=5,color='grey',alpha=0.6)
        	#ax.axvline( 31,linestyle='--',dashes=(25,15),lw=5,color='grey',alpha=0.6)
        	ax.fill_between([41.36,50],y1=[0,0],y2=np.array([1,1])*np.log10(1./(cosmo.differential_comoving_volume(3.).value*1.* 9.2*(1./60./180.*np.pi)**2)),color='seagreen',alpha=0.3)


###############################################################

plot_snap(snapnum)

prop = matplotlib.font_manager.FontProperties(size=27)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=False)

ax.text(0.85, 0.92, r'$\rm z='+str(redshift)+'$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

if snapnum == 25:
	ax.text(0.75, 0.7, r'$\sim$$73/{\rm arcmin}^{2}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40, color='dimgray')

ax.set_xlim(40.7,bmax+0.2)
ax.set_ylim(-6.35,-0.75)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(L_{{\rm H}_{\beta}+{\rm OIII}}[{\rm erg}/{\rm s}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\phi[{\rm cMpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'Hbeta_lf_'+str(snapnum)+'.pdf',format='pdf')
