import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = " "
from data import *
from line_module import *
import matplotlib.font_manager
from astropy.cosmology import FlatLambdaCDM
cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

snapnum = 33
redshift = int(np.array(Zs)[np.array(Snaps)==snapnum])

matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.11,0.12,0.79,0.83])

def addlum(x,y):
	return np.log10(10**x+10**y)

bmin=40.5
bmax=44.5
n_bins=20
bins=np.linspace(bmin,bmax,n_bins)
cx= (bins[1:]+bins[:-1])/2.

def cal_LF(logL,boxlength):
        result1,b=np.histogram(logL,bins=bins)
        n1=result1
        lenbin=b[5]-b[4]
        result1=result1/lenbin/((boxlength/1000./hubble)**3)
        result1=np.log10(result1)
        return result1, n1

def plot_snap(snapnum,obs=True):
	fname="outpath/TNG50-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname) 
	logL=f.root.line_luminosity[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	lineid1 = np.array(linenames) == "Hbeta"
	lineid1 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid1]) 
	lineid2 = np.array(linenames) == "OIII"
	lineid2 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid2])
	lineid3 = np.array(linenames) == "OIII+Hbeta"
	lineid3 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid3])	
	lineid4 = np.array(linenames) == "OIII2"
        lineid4 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid3])
	lineid5 = np.array(linenames) == "OIII+Hbeta2"
        lineid5 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid3])


	logL[np.invert(np.isfinite(logL))]=0

	LF_50_1,N_50_1=cal_LF(addlum(logL[:,lineid1,1],logL[:,lineid2,1]),35000.)
	ax.plot(cx,LF_50_1,'-',c='crimson', label=r'$\rm with$ $\rm all$ $\rm dust$')

	LF_50_2,N_50_2=cal_LF(addlum(logL[:,lineid1,0],logL[:,lineid2,0]),35000.)
	ax.plot(cx,LF_50_2,'--',c='crimson', label=r'$\rm without$ $\rm resolved$ $\rm dust$')

	LF_50_1,N_50_1=cal_LF(addlum(logL[:,lineid1,1],logL[:,lineid4,1]),35000.)
	ax.plot(cx,LF_50_1,'-',c='royalblue')
	LF_50_2,N_50_2=cal_LF(addlum(logL[:,lineid1,0],logL[:,lineid4,0]),35000.)
	ax.plot(cx,LF_50_2,'--',c='royalblue')

	LF_50_1,N_50_1=cal_LF(logL[:,lineid3,1],35000.)
        ax.plot(cx,LF_50_1,'-',c='seagreen')
        LF_50_2,N_50_2=cal_LF(logL[:,lineid3,0],35000.)
        ax.plot(cx,LF_50_2,'--',c='seagreen')

	LF_50_1,N_50_1=cal_LF(logL[:,lineid5,1],35000.)
        ax.plot(cx,LF_50_1,'-',c='chocolate')
        LF_50_2,N_50_2=cal_LF(logL[:,lineid5,0],35000.)
        ax.plot(cx,LF_50_2,'--',c='chocolate')
	

###############################################################

plot_snap(snapnum)

prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

ax.text(0.85, 0.92, r'$\rm z='+str(redshift)+'$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

ax.set_xlim(40.7,bmax+0.2)
ax.set_ylim(-6.35,-0.75)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(L_{{\rm H}_{\beta}+{\rm OIII}}[{\rm erg}/{\rm s}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
plt.show()
