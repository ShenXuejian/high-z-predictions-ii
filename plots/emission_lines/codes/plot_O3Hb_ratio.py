import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager

from line_module import *
from magcorr_module import *
from magcorr_module_nmap import *

bmin=39.
bmax=44.
n_bins=24
bins=np.linspace(bmin,bmax,n_bins)
cx= (bins[1:]+bins[:-1])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
        '33' :{'TNG50-1' :np.array([[38,43.0],  [38,43.0]]),
               'TNG100-1':np.array([[41.6,43.5],[41.6,43.5]]),
               'TNG300-1':np.array([[42.3,45],  [42.3,45]])},
        '25' :{'TNG50-1' :np.array([[38,43.0],  [38,43.4]]),
               'TNG100-1':np.array([[41.4,43.5],[41.4,43.5]]),
               'TNG300-1':np.array([[42.3,45],  [42.3,45]])},
        '21' :{'TNG50-1' :np.array([[38,43.0],  [38,43.4]]),
               'TNG100-1':np.array([[41.5,43.5],[41.5,43.5]]),
               'TNG300-1':np.array([[42.5,45],  [42.5,45]])},
        '17' :{'TNG50-1' :np.array([[38,43.0],  [38,43.4]]),
               'TNG100-1':np.array([[41.4,43.5],[41.4,43.5]]),
               'TNG300-1':np.array([[42.5,45],  [42.5,45]])},
}

def combine(v50,v100,v300,n50,n100,n300,snapnum,boundnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][boundnum,0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][boundnum,1]) 
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][boundnum,0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][boundnum,1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][boundnum,0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][boundnum,1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def plt_data(snapnum,redshift,model):
	
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()
	
	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C
	
	fname="outpath/TNG50-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	lineid1 = np.array(linenames) == "OIII"
	lineid1 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid1])
	lineid2 = np.array(linenames) == "Hbeta"
	lineid2 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid2])

	logL[np.invert(np.isfinite(logL))]=0

	ratio_i = logL[:,lineid1,0] - logL[:,lineid2,0]
	logHb_i = logL[:,lineid2,0]
	ratio_d = logL[:,lineid1,1] - logL[:,lineid2,1]
	logHb_d = logL[:,lineid2,1]

	bestfit=best_fits[model][str(snapnum)]+1	
	valid = np.isfinite(ratio_d) & (ratio_d > -5) & (ratio_d < 5)

	num50,b,_=st.binned_statistic(logHb_d[valid], ratio_d[valid] , statistic='count', bins=bins)
	med50,b,_=st.binned_statistic(logHb_d[valid], ratio_d[valid] ,statistic='median', bins=bins)
	sigma50,b,_=st.binned_statistic(logHb_d[valid], ratio_d[valid] , statistic=std, bins=bins)
		
	id=(num50>10)
	ax.plot(cx[id],med50[id],linestyle='-',lw=4 ,c='royalblue')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')
		
	num50i,b,_=st.binned_statistic(logHb_i[valid],ratio_i[valid] , statistic='count', bins=bins)
	med50i,b,_=st.binned_statistic(logHb_i[valid],ratio_i[valid] , statistic='median', bins=bins)
	#id=(num50i>10)
	#ax.plot(cx[id],med50i[id],':',lw=6,c='royalblue',alpha=0.6)
	
	fname="outpath/TNG100-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	lineid1 = np.array(linenames) == "OIII"
	lineid1 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid1])
	lineid2 = np.array(linenames) == "Hbeta"
	lineid2 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid2])

	logL[np.invert(np.isfinite(logL))]=0

	halomass=mhalo['TNG100-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,1))
	logL[:,lineid2,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,0))
	logL[:,lineid2,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,1))

	ratio_i = logL[:,lineid1,0] - logL[:,lineid2,0]
	logHb_i = logL[:,lineid2,0]
	ratio_d = logL[:,lineid1,1] - logL[:,lineid2,1]
	logHb_d = logL[:,lineid2,1]

	bestfit=best_fits[model][str(snapnum)]+1
	valid = np.isfinite(ratio_d) & (ratio_d > -5) & (ratio_d < 5)
	num100,b,_=st.binned_statistic(logHb_d[valid],ratio_d[valid] , statistic='count', bins=bins)
	med100,b,_=st.binned_statistic(logHb_d[valid],ratio_d[valid] ,statistic='median', bins=bins)
	sigma100,b,_=st.binned_statistic(logHb_d[valid], ratio_d[valid] , statistic=std, bins=bins)

	id=(num100>10)
	ax.plot(cx[id],med100[id],linestyle='-',lw=4 ,c='crimson')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')

	num100i,b,_=st.binned_statistic(logHb_i[valid],ratio_i[valid], statistic='count', bins=bins)
	med100i,b,_=st.binned_statistic(logHb_i[valid],ratio_i[valid],statistic='median', bins=bins)
	#id=(num100i>10)
	#ax.plot(cx[id],med100i[id],':',lw=6,c='crimson',alpha=0.6)
		
	fname="outpath/TNG300-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	lineid1 = np.array(linenames) == "OIII"
	lineid1 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid1])
	lineid2 = np.array(linenames) == "Hbeta"
	lineid2 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid2])

	logL[np.invert(np.isfinite(logL))]=0

	halomass=mhalo['TNG300-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,1))
	logL[:,lineid2,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,0))
	logL[:,lineid2,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,1))

	ratio_i = logL[:,lineid1,0] - logL[:,lineid2,0]
	logHb_i = logL[:,lineid2,0]
	ratio_d = logL[:,lineid1,1] - logL[:,lineid2,1]
	logHb_d = logL[:,lineid2,1]

	bestfit=best_fits[model][str(snapnum)]+1
	valid = np.isfinite(ratio_d) & (ratio_d > -5) & (ratio_d < 5)
	num300,b,_=st.binned_statistic(logHb_d[valid],ratio_d[valid] , statistic='count', bins=bins)
	med300,b,_=st.binned_statistic(logHb_d[valid],ratio_d[valid] ,statistic='median', bins=bins)
	sigma300,b,_=st.binned_statistic(logHb_d[valid], ratio_d[valid] , statistic=std, bins=bins)

	id=(num300>10)
	ax.plot(cx[id],med300[id],linestyle='-',lw=4 ,c='seagreen')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')

	num300i,b,_=st.binned_statistic(logHb_i[valid],ratio_i[valid], statistic='count', bins=bins)
	med300i,b,_=st.binned_statistic(logHb_i[valid],ratio_i[valid],statistic='median', bins=bins)
	#id=(num300i>10)
	#ax.plot(cx[id],med300i[id],':',lw=6,c='seagreen',alpha=0.6)

	med_combined  = combine(med50,med100,med300,num50,num100,num300,snapnum,0)
	sig_combined  = combine(sigma50,sigma100,sigma300,num50,num100,num300,snapnum,0)
	if snapnum==25: med_combined[sig_combined==0]=np.nan
	ax.errorbar(cx,med_combined,yerr=sig_combined,linestyle='-',c='royalblue',mec='royalblue',marker='o',ms=15,capsize=9,capthick=4,label=r'$\rm with$ $\rm all$ $\rm dust$')
	medi_combined = combine(med50i,med100i,med300i,num50i,num100i,num300i,snapnum,1)
	#ax.plot(cx,medi_combined,':',c='royalblue',label=r'$\rm without$ $\rm resolved$ $\rm dust$')

	##############################################
#fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,30))
#row=3
#col=1
redshifts=[2,3,4,5]
snapnums=[33,25,21,17]

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(4):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=27)
	if i in [0]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.10, 0.92, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_ylim(0.41,1.47)
	ax.set_xlim(39.1,42.8)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_xlabel(r'$\log{(L_{{\rm H}_{\beta}}[{\rm erg}{\rm s}^{-1}])}$',fontsize=40,labelpad=2.5)
	ax.set_ylabel(r'$\log{(L_{[OIII]}/L_{{\rm H}_{\beta}})}$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'OIIItoHbeta_vs_Hbeta_'+str(snapnums[i])+'.pdf',fmt='pdf')

