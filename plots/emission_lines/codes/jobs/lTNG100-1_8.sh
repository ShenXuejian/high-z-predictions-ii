#!/bin/sh
#SBATCH -p vogelsberger
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=64
#SBATCH --mail-user=xuejian@mit.edu
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mem-per-cpu=4000
#SBATCH -t 7-00:00           # Runtime in D-HH:MM

cd ..
[ -r get_emission_strength_new.py ] || {
	echo "wrong code path"
	exit
}
mpirun -n 128 python get_emission_strength_new.py TNG100-1 8 > ./logs/lTNG100-1_8.log
