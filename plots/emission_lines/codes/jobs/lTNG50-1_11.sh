#!/bin/sh
#SBATCH -p vogelsberger
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=64
#SBATCH --exclusive
#SBATCH --mail-user=xuejian@mit.edu
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mem-per-cpu=4000
#SBATCH -t 7-00:00           # Runtime in D-HH:MM

cd ..
[ -r get_emission_strength_new.py ] || {
	echo "wrong code path"
	exit
}
mpirun -n 128 python get_emission_strength_new.py TNG50-1 11 > ./logs/lTNG50-1_11.log
