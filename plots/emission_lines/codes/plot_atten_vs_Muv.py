import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
from curves import *
import matplotlib.font_manager

from line_module import *
from magcorr_module import *
from mstarcorr_module import *
from sfrcorr_module import *

bins=np.linspace(-24,-16, 24)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
        '33' :{'TNG50-1':[-16,-20.0],'TNG100-1':[-18.2,-21.5],'TNG300-1':[-19.8,-24]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter<=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter<=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter<=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def addlum(x,y):
        return np.log10(10**x+10**y)

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def plt_data(snapnum,redshift,model):	
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()
	
	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C

	fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	f.close()
	
	bestfit=best_fits[model][str(snapnum)]+1
	Auv = mags[:,bestfit] - mags[:,0]
	Auv[Auv<0]=0

	#apparent band attenuation, not used in final version
	fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+"_ap.hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags_ap= f.root.band_magnitudes[:,:,:].copy()
        f.close()

	A150= mags_ap[:,12,bestfit] - mags_ap[:,12,0]
	A150[A150<0]=0
	A200= mags_ap[:,13,bestfit] - mags_ap[:,13,0] 
	A200[A200<0]=0

	fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()

        fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()

        ssfr = np.log10(sfr/mstar)

	fname="outpath/TNG50-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	lineid1 = np.array(linenames) == "Halpha"
	lineid1 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid1])
	lineid2 = np.array(linenames) == "Hbeta"
	lineid2 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid2])
	lineid3 = np.array(linenames) == "OIII2"
        lineid3 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid3])
	lineid4 = np.array(linenames) == "OIII+Hbeta_2"
        lineid4 = int(np.arange(0,len(linenames),dtype=np.int32)[lineid4])

	#equivalent width, not used
	#logEW = np.log10(EW)

	f=-0.296*np.log10(EW[:,lineid1,1])+0.8
	f[f<0]=0
	logL[:,lineid1,0] = logL[:,lineid1,0] + np.log10(1./(1.+f))
	logL[:,lineid1,1] = logL[:,lineid1,1] + np.log10(1./(1.+f))

	#logEW_Ha = logEW[:,lineid1,1]
	#logEW_Hb = logEW[:,lineid2,1]
	#logEW_O3 = logEW[:,lineid3,1]
	A_Ha = 2.5*(logL[:,lineid1,0] - logL[:,lineid1,1])
	#A_O3 = 2.5*(logL[:,lineid4,0] - logL[:,lineid4,1])
	A_O3 = 2.5*(addlum(logL[:,lineid2,0],logL[:,lineid3,0]) - addlum(logL[:,lineid2,1],logL[:,lineid3,1]))
	A_Ha[A_Ha<0]=0
	A_O3[A_O3<0]=0

	bestfit=best_fits[model][str(snapnum)]+1	
	valid = np.isfinite(Auv) & np.isfinite(A_Ha) & np.isfinite(A_O3)

	num50_1,b,_=st.binned_statistic(mags[valid,bestfit], A_Ha[valid] , statistic='count', bins=bins)
	med50_1,b,_=st.binned_statistic(mags[valid,bestfit], A_Ha[valid] ,statistic='median', bins=bins)
	sigma50_1,b,_=st.binned_statistic(mags[valid,bestfit], A_Ha[valid] , statistic=std, bins=bins)	
	#id=(num50_1>10)
	#ax.plot(cx[id],med50_1[id],linestyle='-',lw=4 ,c='royalblue', label=r'${\rm H}_{\alpha}$')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')

	num50_2,b,_=st.binned_statistic(mags[valid,bestfit], A_O3[valid] , statistic='count', bins=bins)
        med50_2,b,_=st.binned_statistic(mags[valid,bestfit], A_O3[valid] ,statistic='median', bins=bins)
        sigma50_2,b,_=st.binned_statistic(mags[valid,bestfit], A_O3[valid] , statistic=std, bins=bins)
        #id=(num50_3>10)
        #ax.plot(cx[id],med50_3[id],linestyle='-',lw=4 ,c='darkorchid', label=r'${\rm O}_{\rm [III]}$')

	num50_3,b,_=st.binned_statistic(mags[valid,bestfit], Auv[valid] , statistic='count', bins=bins)
        med50_3,b,_=st.binned_statistic(mags[valid,bestfit], Auv[valid] ,statistic='median', bins=bins)
        sigma50_3,b,_=st.binned_statistic(mags[valid,bestfit], Auv[valid] , statistic=std, bins=bins)
        #id=(num50_4>10)
        #ax.plot(cx[id],med50_4[id],linestyle='-',lw=4 ,c='gray', label=r'${\rm UV}$')

	num50_4,b,_=st.binned_statistic(mags[valid,bestfit], A150[valid] , statistic='count', bins=bins)
        med50_4,b,_=st.binned_statistic(mags[valid,bestfit], A150[valid] ,statistic='median', bins=bins)
        sigma50_4,b,_=st.binned_statistic(mags[valid,bestfit], A150[valid] , statistic=std, bins=bins)
	
	num50_5,b,_=st.binned_statistic(mags[valid,bestfit], A200[valid] , statistic='count', bins=bins)
        med50_5,b,_=st.binned_statistic(mags[valid,bestfit], A200[valid] ,statistic='median', bins=bins)
        sigma50_5,b,_=st.binned_statistic(mags[valid,bestfit], A200[valid] , statistic=std, bins=bins)
	#########################

	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	halomass=mhalo['TNG100-1'][subids]
	if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
	else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
	f.close()
	Auv = mags[:,bestfit] - mags[:,0]
        Auv[Auv<0]=0
	
	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+"_ap.hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags_ap= f.root.band_magnitudes[:,:,:].copy()
        halomass=mhalo['TNG100-1'][subids]
	f.close()
	for i in range(mags_ap.shape[1]):
        	mags_ap[:,i,:] = mags_ap[:,i,:] + get_corr_ap(halomass,'TNG100-1',snapnum,model,band=i)
        A150 = mags_ap[:,12,bestfit] - mags_ap[:,12,0]
        A150[A150<0]=0
	A200 = mags_ap[:,13,bestfit] - mags_ap[:,13,0]
        A200[A200<0]=0

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()
        mstar=mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)

        fname=sfrPath+'TNG100-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG100-1',snapnum)

        ssfr = np.log10(sfr/mstar)

	fname="outpath/TNG100-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	logEW = np.log10(EW)

	f=-0.296*np.log10(EW[:,lineid1,1])+0.8
	f[f<0]=0
	logL[:,lineid1,0] = logL[:,lineid1,0] + np.log10(1./(1.+f))
	logL[:,lineid1,1] = logL[:,lineid1,1] + np.log10(1./(1.+f))

	halomass=mhalo['TNG100-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,1))
	logL[:,lineid2,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,0))
	logL[:,lineid2,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,1))
	logL[:,lineid3,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid3,0))
        logL[:,lineid3,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid3,1))
	logL[:,lineid4,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid4,0))
        logL[:,lineid4,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid4,1))

	logEW[:,lineid1,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,0),"ew")
        logEW[:,lineid1,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid1,1),"ew")
        logEW[:,lineid2,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,0),"ew")
        logEW[:,lineid2,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid2,1),"ew")
	logEW[:,lineid3,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid3,0),"ew")
        logEW[:,lineid3,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid3,1),"ew")
	logEW[:,lineid4,0] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid4,0),"ew")
        logEW[:,lineid4,1] += get_corr_line(halomass,'TNG100-1',snapnum, (lineid4,1),"ew")

	logEW_Ha = logEW[:,lineid1,1]
        logEW_Hb = logEW[:,lineid2,1]
        logEW_O3 = logEW[:,lineid3,1]
	A_Ha = 2.5*(logL[:,lineid1,0] - logL[:,lineid1,1])
        #A_O3 = 2.5*(logL[:,lineid4,0] - logL[:,lineid4,1])
        A_O3 = 2.5*(addlum(logL[:,lineid2,0],logL[:,lineid3,0]) - addlum(logL[:,lineid2,1],logL[:,lineid3,1]))
        A_Ha[A_Ha<0]=0
        A_O3[A_O3<0]=0

        bestfit=best_fits[model][str(snapnum)]+1
	valid = np.isfinite(Auv) & np.isfinite(A_Ha) & np.isfinite(A_O3)

	num100_1,b,_=st.binned_statistic(mags[valid,bestfit],A_Ha[valid] , statistic='count', bins=bins)
	med100_1,b,_=st.binned_statistic(mags[valid,bestfit],A_Ha[valid] ,statistic='median', bins=bins)
	sigma100_1,b,_=st.binned_statistic(mags[valid,bestfit],A_Ha[valid] , statistic=std, bins=bins)
	#id=(num100_1>10)
	#ax.plot(cx[id],med100_1[id],linestyle='--',lw=4 ,c='royalblue', alpha=0.5)
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')
	
	num100_2,b,_=st.binned_statistic(mags[valid,bestfit],A_O3[valid] , statistic='count', bins=bins)
        med100_2,b,_=st.binned_statistic(mags[valid,bestfit],A_O3[valid] ,statistic='median', bins=bins)
        sigma100_2,b,_=st.binned_statistic(mags[valid,bestfit],A_O3[valid] , statistic=std, bins=bins)
        #id=(num100_3>10)
        #ax.plot(cx[id],med100_3[id],linestyle='--',lw=4 ,c='darkorchid', alpha=0.5)

	num100_3,b,_=st.binned_statistic(mags[valid,bestfit],Auv[valid] , statistic='count', bins=bins)
        med100_3,b,_=st.binned_statistic(mags[valid,bestfit],Auv[valid] ,statistic='median', bins=bins)
        sigma100_3,b,_=st.binned_statistic(mags[valid,bestfit],Auv[valid] , statistic=std, bins=bins)
        #id=(num100_4>10)
        #ax.plot(cx[id],med100_4[id],linestyle='--',lw=4 ,c='gray', alpha=0.5)

	num100_4,b,_=st.binned_statistic(mags[valid,bestfit],A150[valid] , statistic='count', bins=bins)
        med100_4,b,_=st.binned_statistic(mags[valid,bestfit],A150[valid] ,statistic='median', bins=bins)
        sigma100_4,b,_=st.binned_statistic(mags[valid,bestfit],A150[valid] , statistic=std, bins=bins)
	
	num100_5,b,_=st.binned_statistic(mags[valid,bestfit],A200[valid] , statistic='count', bins=bins)
        med100_5,b,_=st.binned_statistic(mags[valid,bestfit],A200[valid] ,statistic='median', bins=bins)
        sigma100_5,b,_=st.binned_statistic(mags[valid,bestfit],A200[valid] , statistic=std, bins=bins)

	fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	halomass=mhalo['TNG300-1'][subids]
	if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG300-1',snapnum,model)
	else: mags=mags+get_corr(halomass,'TNG300-1',snapnum,model)
	f.close()	
	Auv = mags[:,bestfit] - mags[:,0]
	Auv[Auv<0]=0

	fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+"_ap.hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags_ap= f.root.band_magnitudes[:,:,:].copy()
        halomass=mhalo['TNG300-1'][subids]
        f.close()
        for i in range(mags_ap.shape[1]):
                mags_ap[:,i,:] = mags_ap[:,i,:] + get_corr_ap(halomass,'TNG300-1',snapnum,model,band=i)
        A150 = mags_ap[:,12,bestfit] - mags_ap[:,12,0]
        A150[A150<0]=0
        A200 = mags_ap[:,13,bestfit] - mags_ap[:,13,0]
        A200[A200<0]=0

	fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()
        mstar=mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)

        fname=sfrPath+'TNG300-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG300-1',snapnum)

        ssfr = np.log10(sfr/mstar)

	fname="outpath/TNG300-1/line_luminosity_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	logL=f.root.line_luminosity[:].copy()
	EW=f.root.equivalent_width[:].copy()
	subids=f.root.subids[:].copy()
	linenames=f.root.emission_line_names[:]
	f.close()

	#logL[np.invert(np.isfinite(logL))]=0
	logEW = np.log10(EW)

	f=-0.296*np.log10(EW[:,lineid1,1])+0.8
	f[f<0]=0
	logL[:,lineid1,0] = logL[:,lineid1,0] + np.log10(1./(1.+f))
	logL[:,lineid1,1] = logL[:,lineid1,1] + np.log10(1./(1.+f))

	halomass=mhalo['TNG300-1'][subids]
	logL[:,lineid1,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,0))
	logL[:,lineid1,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,1))
	logL[:,lineid2,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,0))
	logL[:,lineid2,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,1))
	logL[:,lineid3,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid3,0))
        logL[:,lineid3,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid3,1))
	logL[:,lineid4,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid4,0))
        logL[:,lineid4,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid4,1))

	logEW[:,lineid1,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,0),"ew")
        logEW[:,lineid1,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid1,1),"ew")
        logEW[:,lineid2,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,0),"ew")
        logEW[:,lineid2,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid2,1),"ew")
        logEW[:,lineid3,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid3,0),"ew")
        logEW[:,lineid3,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid3,1),"ew")
	logEW[:,lineid4,0] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid4,0),"ew")
        logEW[:,lineid4,1] += get_corr_line(halomass,'TNG300-1',snapnum, (lineid4,1),"ew")

	logEW_Ha = logEW[:,lineid1,1]
        logEW_Hb = logEW[:,lineid2,1]
        logEW_O3 = logEW[:,lineid3,1]
	A_Ha = 2.5*(logL[:,lineid1,0] - logL[:,lineid1,1])
        #A_O3 = 2.5*(logL[:,lineid4,0] - logL[:,lineid4,1])
        A_O3 = 2.5*(addlum(logL[:,lineid2,0],logL[:,lineid3,0]) - addlum(logL[:,lineid2,1],logL[:,lineid3,1]))
        A_Ha[A_Ha<0]=0
        A_O3[A_O3<0]=0

        bestfit=best_fits[model][str(snapnum)]+1
	valid = np.isfinite(Auv) & np.isfinite(A_Ha) & np.isfinite(A_O3)

	num300_1,b,_=st.binned_statistic(mags[valid,bestfit],A_Ha[valid] , statistic='count', bins=bins)
	med300_1,b,_=st.binned_statistic(mags[valid,bestfit],A_Ha[valid] ,statistic='median', bins=bins)
	sigma300_1,b,_=st.binned_statistic(mags[valid,bestfit],A_Ha[valid] , statistic=std, bins=bins)
	#id=(num300_1>10)
	#ax.plot(cx[id],med300_1[id],linestyle=':',lw=4 ,c='royalblue', alpha=0.3)
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')

	num300_2,b,_=st.binned_statistic(mags[valid,bestfit],A_O3[valid] , statistic='count', bins=bins)
        med300_2,b,_=st.binned_statistic(mags[valid,bestfit],A_O3[valid] ,statistic='median', bins=bins)
        sigma300_2,b,_=st.binned_statistic(mags[valid,bestfit],A_O3[valid] , statistic=std, bins=bins)
        #id=(num300_3>10)
        #ax.plot(cx[id],med300_3[id],linestyle=':',lw=4 ,c='darkorchid', alpha=0.3)

	num300_3,b,_=st.binned_statistic(mags[valid,bestfit], Auv[valid] , statistic='count', bins=bins)
        med300_3,b,_=st.binned_statistic(mags[valid,bestfit], Auv[valid] ,statistic='median', bins=bins)
        sigma300_3,b,_=st.binned_statistic(mags[valid,bestfit], Auv[valid] , statistic=std, bins=bins)
        #id=(num300_4>10)
        #ax.plot(cx[id],med300_4[id],linestyle=':',lw=4 ,c='gray', alpha=0.3)

	num300_4,b,_=st.binned_statistic(mags[valid,bestfit], A150[valid] , statistic='count', bins=bins)
        med300_4,b,_=st.binned_statistic(mags[valid,bestfit], A150[valid] ,statistic='median', bins=bins)
        sigma300_4,b,_=st.binned_statistic(mags[valid,bestfit], A150[valid] , statistic=std, bins=bins)

	num300_5,b,_=st.binned_statistic(mags[valid,bestfit], A200[valid] , statistic='count', bins=bins)
        med300_5,b,_=st.binned_statistic(mags[valid,bestfit], A200[valid] ,statistic='median', bins=bins)
        sigma300_5,b,_=st.binned_statistic(mags[valid,bestfit], A200[valid] , statistic=std, bins=bins)

	med_combined  = combine(med50_1,med100_1,med300_1,num50_1,num100_1,num300_1,snapnum)
	sig_combined  = combine(sigma50_1,sigma100_1,sigma300_1,num50_1,num100_1,num300_1,snapnum)
	ax.plot(cx,med_combined,linestyle='-',c='crimson',mec='crimson',marker='o',ms=15,label=r'${\rm H}_{\alpha}$')
	#ax.errorbar(cx,med_combined,yerr=sig_combined,linestyle='-',c='royalblue',mec='royalblue',marker='o',ms=15,label=r'${\rm H}_{\alpha}$')
	med_combined  = combine(med50_2,med100_2,med300_2,num50_2,num100_2,num300_2,snapnum)
        sig_combined  = combine(sigma50_2,sigma100_2,sigma300_2,num50_2,num100_2,num300_2,snapnum)
        ax.plot(cx,med_combined,linestyle='-',c='royalblue',mec='royalblue',marker='o',ms=15,label=r'${\rm H}_{\beta}+[{\rm O\,III}]$')
	med_combined  = combine(med50_3,med100_3,med300_3,num50_3,num100_3,num300_3,snapnum)
        sig_combined  = combine(sigma50_3,sigma100_3,sigma300_3,num50_3,num100_3,num300_3,snapnum)
        ax.plot(cx,med_combined,linestyle='-',c='darkorchid',mec='darkorchid',marker='o',ms=15,label=r'${\rm UV}$')
	'''
	med_combined  = combine(med50_4,med100_4,med300_4,num50_4,num100_4,num300_4,snapnum)
        sig_combined  = combine(sigma50_4,sigma100_4,sigma300_4,num50_4,num100_4,num300_4,snapnum)
        ax.plot(cx,med_combined,linestyle='--',dashes=(25,15),c='royalblue',mec='royalblue',marker='o',ms=15)
	med_combined  = combine(med50_5,med100_5,med300_5,num50_5,num100_5,num300_5,snapnum)
        sig_combined  = combine(sigma50_5,sigma100_5,sigma300_5,num50_5,num100_5,num300_5,snapnum)
        ax.plot(cx,med_combined,linestyle='--',dashes=(25,15),c='crimson',mec='crimson',marker='o',ms=15)
	'''
	factor = Curve_Calzetti(6563.)/Curve_Calzetti(1450.)
	ax.plot(cx,med_combined*factor,linestyle='--',dashes=(25,15),c='crimson',mec='crimson',label=r'${\rm H}_{\alpha}$ $\rm expected$') 
	factor = Curve_Calzetti(5000.)/Curve_Calzetti(1450.)
        ax.plot(cx,med_combined*factor,linestyle='--',dashes=(25,15),c='royalblue',mec='royalblue',label=r'${\rm H}_{\beta}+[{\rm O\,III}]$ $\rm expected$')

	##############################################
#fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,30))
#row=3
#col=1
redshifts=[2]
snapnums=[33]

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(1):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=27)
	if i in [0]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=2,frameon=False)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.90, 0.08, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_ylim(0.,2.05)
	ax.set_xlim(-17+0.5,-21-0.5)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_ylabel(r'$A_{\rm x}$',fontsize=40,labelpad=2.5)
	ax.set_xlabel(r'$M_{\rm UV}$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'atten_vs_Muv_'+str(snapnums[i])+'.pdf',fmt='pdf')

