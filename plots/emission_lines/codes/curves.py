import numpy as np 

########################################################################
# dust attenuation, x represent lambda
# MW extinction curve(Cardelli1989) + a correction on scattering(Calzetti1994), Nelson2018
# have extrapolated to wavelengths outside the original extinction curve
def a(x):
	if x>0 and x<=1.1: return 0.574*np.power(x,1.61)
	elif x>1.1 and x<=3.3:
		y=x-1.82
		return 1+0.17699*y-0.50447*y**2-0.02427*y**3+0.72085*y**4+0.01979*y**5-0.77530*y**6+0.32999*y**7 
	elif x>3.3 and x<=8: 
		if x>=5.9 and x<=8: Fax=-0.04473*(x-5.9)**2-0.009779*(x-5.9)**3
		else: Fax=0
		return 1.752-0.316*x-0.104/((x-4.67)**2+0.341)+Fax
	elif x>8:
		return -1.073-0.628*(x-8)+0.137*(x-8)**2-0.070*(x-8)**3
	else: return 0

def b(x):
	if x>0 and x<=1.1: return -0.527*np.power(x,1.61)
	elif x>1.1 and x<=3.3:
		y=x-1.82	
		return 1.41338*y+2.28305*y**2+1.07233*y**3-5.38434*y**4-0.62251*y**5+5.30260*y**6-2.09002*y**7
	elif x>3.3 and x<=8: 
		if x>=5.9 and x<=8: Fbx=0.2130*(x-5.9)**2+0.1207*(x-5.9)**3
		else: Fbx=0
		return -3.090+1.825*x+1.206/((x-4.62)**2+0.263)+Fbx
	elif x>8:
		return 13.670+4.257*(x-8)-0.420*(x-8)**2+0.374*(x-8)**3
	else: return 0 

def Curve_MW(x):   #A(lambda)/A(V),  input x unit A
	x=10000./x
	Rv=3.1  #define as A(V)/E(B-V), 3.1 is the default choice for MW extinction curve
	return a(x)+b(x)/Rv

def w_sc(x):
	y=np.log10(x)
	if x>0 and x<=3460: return 0.43+0.366*(1-np.exp(-(y-3)**2/0.2))
	else: return -0.48*y+2.41

def h_sc(x):
	y=np.log10(x)
	return 1-0.561*np.exp(-np.power(np.abs(y-3.3112),2.2)/0.17)

def tau_total(x): #input x unit A
#correction on optical depth to account for photons scattered into line of sight, Calzetti 1994
	return Curve_MW(x)*(h_sc(x)*np.sqrt(1-w_sc(x))+(1-h_sc(x))*(1-w_sc(x)))

def Curve_Nelson(x):
	return (-2.5*np.log10((1-np.exp(-tau_total(x)))/tau_total(x)))/(-2.5*np.log10((1-np.exp(-tau_total(5477)))/tau_total(5477)))

#########################################################################################################
# Kriek&Conroy attenuation curve
def k(x): # Calzetti law, input unit A
	x=x/10000.  #now x in unit \mu m
	if x>0.63: return 2.659*(-1.857+1.040/x)+4.05  
	elif x<=0.63 and x>0: return 2.659*(-2.156+1.509/x-0.198/x**2+0.011/x**3)+4.05
	else: return 0

def Curve_Calzetti(x):
	return k(x)/4.05

def D(x,Eb,Dx): #UV bump shape, unit A
	x0=2175.  #center of UV bump
	return Eb*(x*Dx)**2/((x**2-x0**2)**2+(x*Dx)**2)	

def Curve_Kriek(x,delta):  #Conroy&Kriek attenuation curve, unit A
	Eb=0.85-1.9*delta  #observation relation in Kriek&Conroy_2013
	Dx=350
	return (k(x)+D(x,Eb,Dx))/4.05*np.power(x/5477.,delta)

def Curve_Buat(x):
	Dx=356
	Eb=1.26
	delta=-0.13
	return (k(x)+D(x,Eb,Dx))/4.05*np.power(x/5477.,delta)
