import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = 'TNG50-1'
from data import *
import matplotlib.font_manager
from scipy.optimize import curve_fit

matplotlib.style.use('classic')

smass_bins=np.linspace(7,12.5,23)
cx=(smass_bins[1:]+smass_bins[:-1])/2.
lenbin=cx[5]-cx[4]

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def plt_data(snapnum,redshift,label=False):
	f=tables.open_file(densityPath+"overdensity_TNG50-1_"+str(snapnum)+".hdf5")
	overdensity=f.root.overdensity[:,0].copy()
	subids=f.root.subids[:].copy()
	f.close()

	high_th=np.sort(overdensity)[int(overdensity.shape[0]*0.90)]
	low_th =np.sort(overdensity)[int(overdensity.shape[0]*0.10)]
	midup =np.sort(overdensity)[int(overdensity.shape[0]*0.55)]	
	midlow=np.sort(overdensity)[int(overdensity.shape[0]*0.45)]
	idhigh=overdensity>high_th
	idlow =overdensity<low_th
	idmid =(overdensity<midup) & (overdensity>midlow)

	fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	f.close()

	count50,b=np.histogram(np.log10(mstar[idhigh]),bins=smass_bins)
	phi50= np.log10(count50/lenbin/((boxlength/1000./hubble)**3))
	if label==True: ax.plot(cx,phi50,'-',c='royalblue',label=r'$\rm high$ $\rm density$')
	else: ax.plot(cx,phi50,'-',c='royalblue')	

	count50,b=np.histogram(np.log10(mstar[idlow]),bins=smass_bins)
        phi50= np.log10(count50/lenbin/((boxlength/1000./hubble)**3))
        if label==True: ax.plot(cx,phi50,'-',c='crimson',label=r'$\rm low$ $\rm density$')
        else: ax.plot(cx,phi50,'-',c='crimson')

	count50,b=np.histogram(np.log10(mstar[idmid]),bins=smass_bins)
        phi50= np.log10(count50/lenbin/((boxlength/1000./hubble)**3))
        if label==True: ax.plot(cx,phi50,'-',c='seagreen',label=r'$\rm mid$ $\rm density$')
        else: ax.plot(cx,phi50,'-',c='seagreen')

	#phi_limit= np.log10( 10./lenbin/(boxlength/1000./hubble)**3 )
	#id= np.isfinite(phi_combined) & (phi_combined>phi_limit)	
	#args,_=curve_fit(mstar_sch,cx[id],phi_combined[id],p0=np.array([-5,10.7,-1.90]))
	#print snapnum,':',args

fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,10))
row=3
col=3

x0,y0,width,height,wspace,hspace=0.05,0.05,0.3,0.3,0,0
for i in range(9):
	nh,nw=2-i/3,i%3
        ax=fig.add_axes([x0+nw*(width+wspace),y0+nh*(height+hspace),width,height])

	if i==0:
		plt_data(Snaps[i],Zs[i],label=True)
	else: 
		plt_data(Snaps[i],Zs[i])

	prop = matplotlib.font_manager.FontProperties(size=30)
	if i==0: 	
		ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

	ax.text(0.85, 0.92, r'${\rm z='+str(Zs[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_xlim(6.75,11.75)
	ax.set_ylim(-4.8,-0.75)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	if (i/row==col-1):
		ax.set_xlabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
	else: ax.set_xticklabels([])
	if i%row==0:
		ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
	else:
		ax.set_yticklabels([])
#plt.show()
plt.savefig(figpath+'env_GSMF.pdf',fmt='pdf')
