import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
from scipy.optimize import curve_fit
import sys
import astropy.constants as con
SIM = ' '
from data import *
import matplotlib.font_manager
#from magcorr_module import *
#from magcorr_module_nmap import *

#bins=np.linspace(-24,-16, 24)
#cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def plt_data(snapnum,redshift,model):
	
	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C
	
	fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	f.close()

	fname=ionPath+'TNG50-1/ionphoton_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	pdensity=f.root.photon_density[:,:].copy()
	f.close()				

	bestfit=best_fits[model][str(snapnum)]+1	
	rho_Luv = 10**(-0.4*mags[:,bestfit])*3631.*1e-23*4*np.pi*(10.*con.pc.value*100)**2 #unit erg/s/Hz
	valid = np.isfinite(np.log10(rho_Luv)) & np.isfinite(np.log10(pdensity[:,0]))
	ax.plot( np.log10(rho_Luv[valid]), np.log10(pdensity[valid,0]*(35./hubble)**3),'.', ms=5 ,c='royalblue')

	
	#def func_linear(x,b):
	#	return x+b
	#arg,_ = curve_fit( func_linear, np.log10(rho_Luv[valid]), np.log10(pdensity[valid,0]))
	#x_fit = np.linspace(20.,30.,100)
	#ax.plot(x_fit, func_linear(x_fit,*arg), c='cyan', label=r'$\rm best$ $\rm fit$')
	#print arg[0], redshift 

	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags= f.root.band_magnitudes[:,0,:].copy()
        f.close()

        fname=ionPath+'TNG100-1/ionphoton_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        pdensity=f.root.photon_density[:,:].copy()
        f.close()

        rho_Luv = 10**(-0.4*mags[:,bestfit])*3631.*1e-23*4*np.pi*(10.*con.pc.value*100)**2 #unit erg/s/Hz
        valid = np.isfinite(np.log10(rho_Luv)) & np.isfinite(np.log10(pdensity[:,0]))
        ax.plot( np.log10(rho_Luv[valid]), np.log10(pdensity[valid,0]*(75/hubble)**3),'.', ms=5 ,c='crimson')

	fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags= f.root.band_magnitudes[:,0,:].copy()
        f.close()

        fname=ionPath+'TNG300-1/ionphoton_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        pdensity=f.root.photon_density[:,:].copy()
        f.close()

        rho_Luv = 10**(-0.4*mags[:,bestfit])*3631.*1e-23*4*np.pi*(10.*con.pc.value*100)**2 #unit erg/s/Hz
        valid = np.isfinite(np.log10(rho_Luv)) & np.isfinite(np.log10(pdensity[:,0]))
        ax.plot( np.log10(rho_Luv[valid]), np.log10(pdensity[valid,0]*(205/hubble)**3),'.', ms=5 ,c='seagreen')

	ax.plot([],[],'.',marker='o',ms=15,c='royalblue',mec='royalblue',label=r'$\rm TNG50$')
	ax.plot([],[],'.',marker='o',ms=15,c='crimson'  ,mec='crimson',  label=r'$\rm TNG100$')
	ax.plot([],[],'.',marker='o',ms=15,c='seagreen' ,mec='seagreen', label=r'$\rm TNG300$')
	
	xfit = np.linspace(25, 35, 100)
	fesc=0.01
	ax.plot( xfit, xfit+25.2 + np.log10(fesc), c='darkorchid', label=r'$\rm Robertson+$ $\rm 2015$')

redshifts = [6]
snapnums = [13]

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(1):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=25)
	if i in [0]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=4,frameon=True)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.10, 0.92, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_ylim(48.2,53.8)
	ax.set_xlim(25.9,30.1)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_xlabel(r'$\log{(L_{\rm UV}\,[{\rm erg}\,{\rm s}^{-1}])}$',fontsize=40,labelpad=2.5)
	ax.set_ylabel(r'$\log{(f_{\rm esc}\,\dot{N}_{\rm ion}\,[{\rm s}^{-1}])}$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'nion_vs_rhouv-'+str(snapnums[i])+'.png',fmt='png')

