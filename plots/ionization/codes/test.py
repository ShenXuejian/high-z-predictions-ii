import numpy as np
from scipy.optimize import curve_fit
import astropy.constants as con
from astropy.cosmology import FlatLambdaCDM
import sys
import os
import tables
SIM=sys.argv[1]
from data import *
snapnum=int(sys.argv[2])

#outpath='/n/mvogelsfs01/sxj/DUST_MODEL/modelC/TUNE_backup/'
#outpath=outpath_C_ndust
if snapnum>13: outpath=outpath_C
else: outpath=outpath_C_nmap

cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

def get_Np(fname,startloc,endloc):	
        data=np.genfromtxt(fname,names=("lamb","fnu"))
        lamb=data['lamb']*1e4 #unit A
        flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
        lum=flux * 1e-23 * 4.* np.pi * (10.*con.pc.value*100)**2 #unit erg/s/Hz

	index = np.arange(0,len(lamb),dtype=np.int32)
	startid = index[(lamb-startloc)>0][0]
	endid   = index[(lamb-endloc)<0][-1]

	target_lamb = lamb[startid:endid+1]
	target_lum = lum[startid:endid+1]  # erg/s/Hz
	target_freq = con.c.value/(target_lamb*1e-10) #Hz

	if len(target_lum[np.invert(np.isfinite(np.log10(target_lum)))])!=0: return -np.inf 

	def func(x,k,b):
		return k*x+b

	arg,_ = curve_fit(func, np.log10(target_freq), np.log10(target_lum))
	LYCslope, LYCscale = arg[0], arg[1]

	n_ion = 10**LYCscale*1e-7/con.h.value/np.abs(LYCslope) * (con.c.value/912e-10)**LYCslope#1/s
	Np_den = n_ion / (boxsize/hubble**3) #1/s/Mpc^3
	
	return Np_den

ids=np.genfromtxt(outpath+SIM+"/snap_"+str(snapnum)+"/subids")

if len(ids.shape)!=0:
	Ntotsubs               = ids.shape[0]
	ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')

	Nps=np.zeros((len(ids),2))
	Nps_global=np.zeros((len(ids),2))

	index                  = (ids % size) == rank
	dosubs                 = ids[index]
	Ndosubs                = dosubs.shape[0]
	doid_in_subgroup       = ids_in_subgroup[index]

	for si in range(0, Ndosubs):	
		print rank, si, Ndosubs, Ntotsubs
        	sys.stdout.flush()
		subnum=dosubs[si]
	        relaid=doid_in_subgroup[si]

		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/nodust_neb_sed.dat'
		if os.path.isfile(fname)==True: 
			Nps[relaid,0] =  get_Np(fname,500, 900.)
			print np.log10(Nps[relaid,0])
		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/dusty0_neb_sed.dat'
                if os.path.isfile(fname)==True: 
			Nps[relaid,1] =  get_Np(fname,500.,900.)
			print np.log10(Nps[relaid,1])		

		print "------------------------------"
	#print rank, "done"
	comm.Barrier()
	comm.Allreduce(Nps,        Nps_global,     op=MPI.SUM)
	#if rank==0: print "alldone"
if rank==0:
	fname="./outpath/"+SIM+"/ionphoton_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "photon_density", Nps_global)

	if len(ids.shape)==0: f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
	else: f.create_array(f.root, "subids", ids.astype(np.uint32))
	f.close()
