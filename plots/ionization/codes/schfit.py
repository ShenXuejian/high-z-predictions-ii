import numpy as np
from scipy.optimize import curve_fit as fit
SIM='TNG300-1'
from data import *
import matplotlib.pyplot as plt
import sys
import tables
model='C'

n_bins=24
bmin=-24
bmax=-16
bins=np.linspace(bmin,bmax,n_bins)
cx=(bins[1:]+bins[:-1])/2.
binscenter = cx

def combine(LFs,Ns,eff):
        def f(lf,n):
                phi=np.power(10.,lf)
                return phi*n**2
        normalization=0.0*binscenter
        combined=0.0*binscenter
        id50 = (binscenter<=eff['TNG50-1'][0]) & (binscenter>=eff['TNG50-1'][1])
        id100= (binscenter<=eff['TNG100-1'][0]) & (binscenter>=eff['TNG100-1'][1])
        id300= (binscenter<=eff['TNG300-1'][0]) & (binscenter>=eff['TNG300-1'][1])

        combined[id50] += f(LFs['TNG50-1'][id50],Ns['TNG50-1'][id50])
        normalization[id50] += Ns['TNG50-1'][id50]**2
        combined[id100] += f(LFs['TNG100-1'][id100],Ns['TNG100-1'][id100])
        normalization[id100] += Ns['TNG100-1'][id100]**2
        combined[id300] += f(LFs['TNG300-1'][id300],Ns['TNG300-1'][id300])
        normalization[id300] += Ns['TNG300-1'][id300]**2

        return np.log10( combined/normalization )

LFs={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
Ns={'TNG50-1':0,'TNG100-1':0,'TNG300-1':0}
effective_ranges={
'C':{   '4' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.0],'TNG300-1':[-22.0,-24]},
        '6' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-22.0,-24]},
        '8' :{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-21.5],'TNG300-1':[-21.2,-24]},
        '11':{'TNG50-1':[-16,-20],'TNG100-1':[-19.5,-21.5],'TNG300-1':[-21.5,-24]},
        '13':{'TNG50-1':[-16,-20],'TNG100-1':[-20.0,-22.0],'TNG300-1':[-21.8,-24]},
        '17':{'TNG50-1':[-16,-20],'TNG100-1':[-19.2,-22.0],'TNG300-1':[-21.0,-24]},
        '21':{'TNG50-1':[-16,-20],'TNG100-1':[-19.5,-22.0],'TNG300-1':[-21.0,-24]},
        '25':{'TNG50-1':[-16,-20],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-20.5,-24]},
        '33':{'TNG50-1':[-16,-20],'TNG100-1':[-18.0,-22.0],'TNG300-1':[-20.0,-24]}}
}

lenbin=8./23.
phi_limit= np.log10( 10./lenbin/(boxlength/1000./hubble)**3 )

i=0
for snapnum in Snaps:
        if snapnum<=13: outpath=outpath_C_nmap
        else: outpath=outpath_C	

	for SIM in ['TNG50-1','TNG100-1','TNG300-1']:
        	if SIM!='TNG50-1':
			fname=outpath+SIM+'/output/corrLF_'+str(snapnum)+'_'+model+'.hdf5'
		else: fname=outpath+SIM+'/output/LF_'+str(snapnum)+'_'+model+'.hdf5'

		with tables.open_file(fname) as f:
			lf_nd =f.root.intrinsic.luminosity_function[:].copy()
			n_nd  =f.root.intrinsic.counts[:].copy()
			cx=f.root.bins_center[:].copy()
		n_nd[np.invert(np.isfinite(lf_nd))]=1e-37
		lf_nd[np.invert(np.isfinite(lf_nd))]=-1e37
		LFs[SIM]=lf_nd
		Ns[SIM] =n_nd

	data = combine(LFs,Ns,effective_ranges['C'][str(snapnum)])
	plt.figure(1)
	plt.plot(cx,data)
	plt.xlim(-16,-24)
	plt.ylim(-6.5,-0.2)
	plt.savefig("./testout/"+str(snapnum)+".png")

	if snapnum>13:
		id=np.isfinite(data) & (data>phi_limit) 
	elif snapnum==13: id=np.isfinite(data) & (data>phi_limit) & (cx<-17.)
	else: id=np.isfinite(data) & (data>phi_limit) & (cx<-17.5)

	p0=( 10**(-3.37-0.19*(Zs[i]-6)), -20.79+0.13*(Zs[i]-6), -1.91-0.11*(Zs[i]-6) )
	args,pcov=fit(single_sch,cx[id],data[id],p0=p0,maxfev=10000)
	print snapnum," : ",np.log10(args[0]),args[1],args[2]
	print np.sqrt(pcov[0,0]),np.sqrt(pcov[1,1]),np.sqrt(pcov[2,2])
	i+=1
