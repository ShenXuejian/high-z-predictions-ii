import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
SIM=''
from data import *
import matplotlib.font_manager
matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4, markersize=15, markeredgewidth=0.0)
matplotlib.rc('axes', linewidth=4)

import scipy.special as spe
import mpmath
import astropy.constants as con
def cumulative(M_lim, Phis, Ms, alpha ):
	if len(Phis.shape)!=0:
		result=0.0*Phis
		for i in range(len(Phis)):
			result[i]= float( Phis[i] * 3631e-23*4*np.pi*(10*con.pc.value*100)**2 * 10**(-0.4*Ms[i]) * mpmath.gammainc(alpha[i]+2., a=np.power(10.,-0.4*(M_lim-Ms[i]))))
		return np.log10(result)
	else: 
		result = Phis * 3631e-23*4*np.pi*(10*con.pc.value*100)**2 * 10**(-0.4*Ms) * mpmath.gammainc(alpha+2., a=np.power(10.,-0.4*(M_lim-Ms)))	
		return np.log10(float(result))

fig=plt.figure(figsize = (15,10))
row=2
col=1
x0,y0,width,height,wspace,hspace=0.11,0.06,0.79,0.45,0.08,0

#ax=fig.add_axes([x0,y0+(1-0)*height+(1-0)*hspace,width,height])
ax=fig.add_axes([0.13,0.12,0.79,0.83])

Mlim=-17

fit=np.genfromtxt("./schechter_fit_C.dat",names=True)
ax.plot(fit["z"],cumulative(Mlim, 10**fit["LogPhis"],fit["Ms"],fit["alpha"]),'-', lw=6, marker='o', ms=20, c='crimson', alpha=0.6 ,label=r'$\rm TNG:$ $\rm with$ $\rm all$ $\rm dust$')

obpath='/n/home11/sxj/dust/plots/schechter_fit/codes/'

data=np.genfromtxt("./obdata/Ketron2015.dat",names=True)
ax.plot( data["z"],data["rhouv"],'-',c='darkorchid', alpha=0.5, label=r'$\rm Ketron+$ $\rm 2015$')

data=np.genfromtxt("./obdata/Bouwens2016_nd.dat",names=True)
select=data["z"]>0
ax.errorbar( data["z"][select], data["rhouv"][select] , xerr=(data["z"][select]-data["left"][select],data["right"][select]-data["z"][select]) , yerr=(data["rhouv"][select]-data["lo"][select],data["up"][select]-data["rhouv"][select]), marker='o',linestyle='none', c='royalblue', mec='royalblue', ms=12, capsize=0, lw=2, label=r'$\rm B16$ $\rm compilation$'+'\n'+r'($\rm dust$ $\rm corrected$)')

data=np.genfromtxt("./obdata/Bouwens2016.dat",names=True)
select=data["z"]>0
ax.errorbar( data["z"][select], data["rhouv"][select] , xerr=(data["z"][select]-data["left"][select],data["right"][select]-data["z"][select]) , yerr=(data["rhouv"][select]-data["lo"][select],data["up"][select]-data["rhouv"][select]), marker='o',linestyle='none', c='crimson', mec='crimson', ms=12, capsize=0, lw=2, label=r'$\rm B16$ $\rm compilation$')

prop = matplotlib.font_manager.FontProperties(size=25)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)
ax.text(0.82, 0.92, r'$M_{\rm UV}^{\rm lim}=-17$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

ax.set_xlim(0.5,11.3)
#ax.set_yscale('log')
ax.set_ylim(23.8,27.5)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\rm redshift$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\rm UVLD$ $[{\rm erg}\,{\rm s}^{-1}\,{\rm Hz}^{-1}\,{\rm Mpc}^{-3}]$',fontsize=40,labelpad=2.5)
#plt.show()
plt.savefig(figpath+'UVLD.pdf',format='pdf')

