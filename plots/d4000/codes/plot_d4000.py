import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager

from magcorr_module import *
from magcorr_module_nmap import *
from sfrcorr_module import *
from mstarcorr_module import *
from d4000corr_module import *

bins=np.linspace(-13,-8, 24)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
	'33' :{'TNG50-1':[-9.5,-7.0],'TNG100-1':[-10.5,-9.0],'TNG300-1':[-13.0,-10.5]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def plt_data(snapnum,redshift,model):
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()
	
	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C
	
	fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	f.close()

	fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()

        fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        ssfr = np.log10(sfr/mstar)
	
	fname='./outpath/TNG50-1/balmer_break_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        D4000=f.root.D4000[:].copy()
	f.close()				

	bestfit=best_fits[model][str(snapnum)]+1	
	valid = np.isfinite(D4000[:,1]) & (mags[:,bestfit]<-17)

	num50,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic='count', bins=bins)
	med50,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic='median', bins=bins)
	sigma50,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic=std, bins=bins)
		
	#id=(num50>10)
	#ax.plot(cx[id],med50[id],linestyle='-',lw=4 ,c='royalblue')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',marker='o',ms=15,capsize=9,capthick=4,mec='royalblue',c='royalblue')
		
	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags= f.root.band_magnitudes[:,0,:].copy()
        halomass=mhalo['TNG100-1'][subids]
        if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
        else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
        f.close()

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()
        mstar=mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)

        fname=sfrPath+'TNG100-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG100-1',snapnum)
        ssfr = np.log10(sfr/mstar)

        fname='./outpath/TNG100-1/balmer_break_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        D4000=f.root.D4000[:].copy()
        f.close()
	D4000[:,0] = D4000[:,0] * get_corr_d4000(halomass, 'TNG100-1', snapnum, 0)
	D4000[:,1] = D4000[:,1] * get_corr_d4000(halomass, 'TNG100-1', snapnum, 1)

        bestfit=best_fits[model][str(snapnum)]+1
        valid = np.isfinite(D4000[:,1]) & (mags[:,bestfit]<-17)

        num100,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic='count', bins=bins)
        med100,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic='median', bins=bins)
        sigma100,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic=std, bins=bins)

	'''
	num100i,b,_=st.binned_statistic(ssfr[valid], D4000[valid,0] , statistic='count', bins=bins)
        med100i,b,_=st.binned_statistic(ssfr[valid], D4000[valid,0] , statistic='median', bins=bins)
        sigma100i,b,_=st.binned_statistic(ssfr[valid], D4000[valid,0] , statistic=std, bins=bins)

        id=(num100i>10)
        ax.plot(cx[id],med100i[id],linestyle='-',lw=4 ,c='darkorchid')
        #ax.errorbar(cx[id],med100[id],yerr=sigma100[id],linestyle='-',marker='o',ms=15,capsize=9,capthick=4,mec='crimson',c='crimson')
	'''

	##################################
	
	xbins = np.linspace(-13,-8, 32)
	ybins = np.linspace(0.8, 1.8, 26)
	density,_,_ = np.histogram2d(ssfr[valid],D4000[valid,1],bins=[xbins, ybins])
	density = np.log10(density)

	maxi = np.max(density[np.isfinite(density)])
	mini = np.min(density[np.isfinite(density)])

	x,y = np.meshgrid( ybins, xbins )
	cmap = plt.get_cmap('YlOrRd')
	plt.pcolormesh(y,x,density, cmap=cmap, norm=matplotlib.colors.Normalize(vmin=mini, vmax=maxi),alpha=0.6, label=r'$\rm distribution$ $\rm of$' + '\n' + r'$\rm TNG100$ $\rm galaxies$')
	
	##################################

	fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags= f.root.band_magnitudes[:,0,:].copy()
        halomass=mhalo['TNG300-1'][subids]
        if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG300-1',snapnum,model)
        else: mags=mags+get_corr(halomass,'TNG300-1',snapnum,model)
        f.close()

        fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()
        mstar=mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)

        fname=sfrPath+'TNG300-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG300-1',snapnum)
        ssfr = np.log10(sfr/mstar)

        fname='./outpath/TNG300-1/balmer_break_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        D4000=f.root.D4000[:].copy()
        f.close()
	D4000[:,0]=D4000[:,0]*get_corr_d4000(halomass, 'TNG300-1', snapnum, 0)
	D4000[:,1]=D4000[:,1]*get_corr_d4000(halomass, 'TNG300-1', snapnum, 1)

        bestfit=best_fits[model][str(snapnum)]+1
        valid = np.isfinite(D4000[:,1]) & (mags[:,bestfit]<-17)

        num300,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic='count', bins=bins)
        med300,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic='median', bins=bins)
        sigma300,b,_=st.binned_statistic(ssfr[valid], D4000[valid,1] , statistic=std, bins=bins)

	num300i,b,_=st.binned_statistic(ssfr[valid], D4000[valid,0] , statistic='count', bins=bins)
        med300i,b,_=st.binned_statistic(ssfr[valid], D4000[valid,0] , statistic='median', bins=bins)
        sigma300i,b,_=st.binned_statistic(ssfr[valid], D4000[valid,0] , statistic=std, bins=bins)

	id=(num300i>10)
        ax.plot(cx[id],med300i[id],linestyle='-',lw=4 ,c='darkorchid')

	'''
	xbins = np.linspace(-13,-8, 32)
        ybins = np.linspace(0.8, 1.8, 26)
        density,_,_ = np.histogram2d(ssfr[valid],D4000[valid,1],bins=[xbins, ybins])
        density = np.log10(density)

        maxi = np.max(density[np.isfinite(density)])
        mini = np.min(density[np.isfinite(density)])

        x,y = np.meshgrid( ybins, xbins )
        cmap = plt.get_cmap('YlOrRd')
        plt.pcolormesh(y,x,density, cmap=cmap, norm=matplotlib.colors.Normalize(vmin=mini, vmax=maxi),alpha=0.7)
	'''
        #id=(num300>10)
        #ax.plot(cx[id],med50[id],linestyle='-',lw=4 ,c='royalblue')
        #ax.errorbar(cx[id],med300[id],yerr=sigma300[id],linestyle='-',marker='o',ms=15,capsize=9,capthick=4,mec='seagreen',c='seagreen')

	med_combined  = combine(med50,med100,med300,num50,num100,num300,snapnum)
        sig_combined  = combine(sigma50,sigma100,sigma300,num50,num100,num300,snapnum)
	med_combined[cx>-8.3] = np.nan
        ax.errorbar(cx,med_combined,yerr=sig_combined,linestyle='-',c='royalblue',mec='royalblue',marker='o',ms=15,capsize=9,capthick=4,label=r'$\rm TNG$')

	##############################################
	data = np.genfromtxt("../obdata/brinch04.dat",names=True)
	ax.plot(data['logsSFR'],data['D4000'], '-', c='k', label=r'$\rm Brinchmann+$ $\rm 2004$ $\rm z=0$')

	data = np.genfromtxt("../obdata/brinch04_contour.dat",names=True) 
	id = data["id"]==0
	ax.fill(data["sSFR"][id], data["D4000"][id], edgecolor='lightgray', linewidth=3, fill=False)
	id = data["id"]==1
        ax.fill(data["sSFR"][id], data["D4000"][id], edgecolor='gray', linewidth=3, fill=False, label=r'$\rm local$ $\rm contour$')
	id = data["id"]==2
        ax.fill(data["sSFR"][id], data["D4000"][id], edgecolor='gray', linewidth=3, fill=False)
	id = data["id"]==3
        ax.fill(data["sSFR"][id], data["D4000"][id], edgecolor='darkgray', linewidth=3, fill=False)

redshifts=[2]
snapnums=[33]

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(1):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=27)
	if i in [0]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=1,frameon=False)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.10, 0.08, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_ylim(0.82,1.98)
	ax.set_xlim(-12,-8)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_xlabel(r'$\log{({\rm sSFR}\,[{\rm yr}^{-1}])}$',fontsize=40,labelpad=2.5)
	ax.set_ylabel(r'$\rm D4000$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'D4000-'+str(snapnums[i])+'.pdf',fmt='pdf')

