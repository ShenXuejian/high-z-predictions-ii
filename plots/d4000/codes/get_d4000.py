import numpy as np
from scipy.integrate import romberg
from scipy.interpolate import interp1d
import astropy.constants as con
import sys
import os
import tables
SIM=sys.argv[1]
from data import *
snapnum=int(sys.argv[2])
from mpi4py import MPI

#outpath='/n/mvogelsfs01/sxj/DUST_MODEL/modelC/TUNE_backup/'
#outpath=outpath_C_ndust
if snapnum>13: outpath=outpath_C
else: outpath=outpath_C_nmap

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def get_d4000(fname):	
        data=np.genfromtxt(fname,names=("lamb","fnu"))
        lamb=data['lamb']*1e-6 #unit m
        flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
        lum=flux * 1e-23 * 4.* np.pi * (10.*con.pc.value*100)**2 #unit erg/s/Hz

        lamb=np.append(np.append(1e-20,lamb),1e10)
        lum=np.append(np.append(0,lum),0)

        map_grid=np.genfromtxt("waves_map.dat")[:,0]
        fall=interp1d(lamb, lum)
        fmap=fall(map_grid)

        frequency = con.c.value/map_grid #unit Hz
        spec = fmap
        f=interp1d(frequency, spec)
        up = romberg(f , 3e8/4250*1e10  , 3e8/4050*1e10 ,  divmax=100)
        lo = romberg(f , 3e8/3950*1e10  , 3e8/3750*1e10 ,  divmax=100)

	if np.isfinite(up) and np.isfinite(lo) and (up!=0) and (lo!=0):
		return up/lo
	else: return np.inf

comm.Barrier()

ids=np.genfromtxt(outpath+SIM+"/snap_"+str(snapnum)+"/subids")

if len(ids.shape)!=0:
	Ntotsubs               = ids.shape[0]
	ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')

	D4000s 	      = np.zeros((len(ids),2))
	D4000s_global = np.zeros((len(ids),2))

	index                  = (ids % size) == rank
	dosubs                 = ids[index]
	Ndosubs                = dosubs.shape[0]
	doid_in_subgroup       = ids_in_subgroup[index]

	for si in range(0, Ndosubs):	
		print rank, si, Ndosubs, Ntotsubs
        	sys.stdout.flush()
		subnum=dosubs[si]
	        relaid=doid_in_subgroup[si]

		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/nodust_neb_sed.dat'
		if os.path.isfile(fname)==True: 
			D4000s[relaid,0] = get_d4000(fname)

		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/dusty0_neb_sed.dat'
                if os.path.isfile(fname)==True: 
			D4000s[relaid,1] = get_d4000(fname)

		print D4000s[relaid,:]

	comm.Barrier()
	comm.Allreduce(D4000s,        D4000s_global,     op=MPI.SUM)

if rank==0:
	fname="./outpath/"+SIM+"/balmer_break_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "D4000", D4000s_global)

	if len(ids.shape)==0: f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
	else: f.create_array(f.root, "subids", ids.astype(np.uint32))
	f.close()

