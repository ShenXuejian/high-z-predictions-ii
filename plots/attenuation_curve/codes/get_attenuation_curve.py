import numpy as np
from scipy.optimize import curve_fit
import astropy.constants as con
import sys
import os
import tables
SIM='TNG50-1'
from data import *
snapnum=int(sys.argv[1])
selection=sys.argv[2]
from mpi4py import MPI

# the data directory
if snapnum>13: outpath=outpath_C
else: outpath=outpath_C_nmap
lamb=np.genfromtxt("waves.dat")[1:]*1e4 # the first line is the number of the grids

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

comm.Barrier()

ids=np.genfromtxt(outpath+SIM+"/snap_"+str(snapnum)+"/subids")  #ids for all subhalos selected in PaperI

fname=outpath+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
f=tables.open_file(fname)
subids=f.root.subids[:].copy()
gmags= f.root.band_magnitudes[:,5,0].copy()  #intrinsic g,r band magnitudes
rmags= f.root.band_magnitudes[:,6,0].copy()
MUV= f.root.band_magnitudes[:,0,1].copy()    #attenuated UV mags
f.close()

color=gmags-rmags

fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
f=tables.open_file(fname)
mstar=f.root.stellarmass[:].copy()
f.close()

if len(ids.shape)!=0:
	if selection=='b':
		index_selection        = (color<=0.2) & (np.log10(mstar)>7.5) & (MUV<-17)
		ids                    = ids[index_selection]
	elif selection=='r':
		index_selection        = (color>0.2) & (np.log10(mstar)>7.5) & (MUV<-17)
		ids                    = ids[index_selection]
	elif selection=='m1':
		index_selection        = (color<=0.2) & (np.log10(mstar)>7.5) & (np.log10(mstar)<8.5) & (MUV<-17)
                ids                    = ids[index_selection]
	elif selection=='m2':
		index_selection        = (color<=0.2) & (np.log10(mstar)>8.5) & (np.log10(mstar)<9.5) & (MUV<-17)
                ids                    = ids[index_selection]
	elif selection=='m3':
		index_selection        = (color<=0.2) & (np.log10(mstar)>9.5) & (np.log10(mstar)<10.5) & (MUV<-17)
                ids                    = ids[index_selection]
	else:
		index_selection        = (np.log10(mstar)>7.5) & (MUV<-17)
		ids                    = ids[index_selection]

	Ntotsubs               = ids.shape[0]
	ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')

	totsed_i = np.zeros((Ntotsubs,1168))
	totsed_d = np.zeros((Ntotsubs,1168))
	totsed_i_global = np.zeros((Ntotsubs,1168))
        totsed_d_global = np.zeros((Ntotsubs,1168))
	weight = np.zeros(Ntotsubs)
	weight_global = np.zeros(Ntotsubs)

	index                  = (ids % size) == rank
	dosubs                 = ids[index]
	Ndosubs                = dosubs.shape[0]
	doid_in_subgroup       = ids_in_subgroup[index]

	for si in range(0, Ndosubs):	
		print rank, si, Ndosubs, Ntotsubs
        	sys.stdout.flush()
		subnum=dosubs[si]
	        relaid=doid_in_subgroup[si]

		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/nodust_neb_sed.dat'
		if os.path.isfile(fname)==True: 
			data = np.genfromtxt(fname,names=['lamb','flux'])
			totsed_i[relaid,:] = data['flux']
		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/dusty0_neb_sed.dat'
                if os.path.isfile(fname)==True: 
			data = np.genfromtxt(fname,names=['lamb','flux'])
			totsed_d[relaid,:] = data['flux']
	comm.Barrier()
	comm.Allreduce(totsed_i,        totsed_i_global,     op=MPI.SUM)
	comm.Allreduce(totsed_d,        totsed_d_global,     op=MPI.SUM)

#totsed_all_i = np.sum(totsed_i_global,axis=0)
#totsed_all_d = np.sum(totsed_d_global,axis=0)
totatt_all = 2.5*np.log10(totsed_i_global/totsed_d_global)  #the attenuation curve of every galaxy (in units of mag)

if rank==0:
	fname="./outpath/"+SIM+"/attenuation_curve_"+str(snapnum)+"_"+selection+".hdf5"
	f=tables.open_file(fname, mode = "w")
	#f.create_array(f.root, "attenuation_curve", 2.5 * np.log10(totsed_all_i/totsed_all_d))
	f.create_array(f.root, "attenuation_curve", totatt_all)
	if len(ids.shape)==0: f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
	else: f.create_array(f.root, "subids", ids.astype(np.uint32))
	f.close()

