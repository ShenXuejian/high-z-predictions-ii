import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.font_manager
from curves import *
import tables
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=2,markersize=20)
matplotlib.rc('axes', linewidth=2)

fig=plt.figure(figsize=(15,10))
ax  = fig.add_axes([0.11,0.12,0.79,0.83])

lamb0=np.linspace(1000,10000,1000)
curve_calzetti=0.0*lamb0
for i in range(len(lamb0)):
        curve_calzetti[i]=Curve_Calzetti(lamb0[i])
func1 = interp1d(lamb0,curve_calzetti)

def func2(x,delta):
        return func1(x)*np.power(x/5477.,delta)

def fit(acme):
	x=np.append(lamb[loc1:loc2],lamb[loc3:loc4])
	y=np.append(acme[loc1:loc2],acme[loc3:loc4])
	args,_ = curve_fit(func2,x,y)
	return args

lamb=np.genfromtxt("waves.dat")[1:]*1e4
Vloc = np.arange(0,len(lamb),dtype=np.int32)[lamb>5510][0]

loc1 = np.arange(0,len(lamb),dtype=np.int32)[lamb>1000][0]
loc2 = np.arange(0,len(lamb),dtype=np.int32)[lamb<1500][-1]
loc3 = np.arange(0,len(lamb),dtype=np.int32)[lamb>3000][0]
loc4 = np.arange(0,len(lamb),dtype=np.int32)[lamb<10000][-1]

with tables.open_file("outpath/TNG50-1/attenuation_curve_33_m3.hdf5") as f:
	ac = f.root.attenuation_curve[:].copy()

print ac.shape
valid_count = 0
ids = (np.ones(ac.shape[0])!=0)
for i in range(ac.shape[0]):
	id_notvalid = np.invert(np.isfinite(ac[i,:])) | (ac[i,:]==0)
	if np.count_nonzero(id_notvalid)==0: valid_count+=1
	else: ids[i]=False
print valid_count

ac = ac[ids,:]
ac = ac/ac[:,Vloc][:,np.newaxis]
acme = np.median(ac,axis=0)
acup=np.percentile(ac,84,axis=0)
aclo=np.percentile(ac,16,axis=0)
ax.plot(lamb/1e4,acme,c='royalblue',lw=4,alpha=0.8,label=r'$\rm TNG:$ $\rm \log{M_{\ast}\in[9.5,10.5]M_{\odot}}$ $\rm z=2$'+'\n'+r'$\rm Draine+$ $\rm 2007$ $\rm dust$ $\rm model$')
ax.fill_between(lamb/1e4,y1=aclo,y2=acup,alpha=0.3,color='royalblue')

print fit(acme), fit(acup), fit(aclo)

with tables.open_file("outpath/TNG50-1/attenuation_curve_33_m2.hdf5") as f:
        ac = f.root.attenuation_curve[:].copy()

print ac.shape
valid_count = 0
ids = (np.ones(ac.shape[0])!=0)
for i in range(ac.shape[0]):
        id_notvalid = np.invert(np.isfinite(ac[i,:])) | (ac[i,:]==0)
        if np.count_nonzero(id_notvalid)==0: valid_count+=1
        else: ids[i]=False
print valid_count

ac = ac[ids,:]
ac = ac/ac[:,Vloc][:,np.newaxis]
acme = np.median(ac,axis=0)
acup=np.percentile(ac,84,axis=0)
aclo=np.percentile(ac,16,axis=0)
ax.plot(lamb/1e4,acme,c='crimson',lw=4,alpha=0.7,label=r'$\rm \log{M_{\ast}\in[8.5,9.5]M_{\odot}}$')
ax.fill_between(lamb/1e4,y1=aclo,y2=acup,alpha=0.2,color='crimson')

print fit(acme), fit(acup), fit(aclo)

with tables.open_file("outpath/TNG50-1/attenuation_curve_33_m1.hdf5") as f:
        ac = f.root.attenuation_curve[:].copy()

print ac.shape
valid_count = 0
ids = (np.ones(ac.shape[0])!=0)
for i in range(ac.shape[0]):
        id_notvalid = np.invert(np.isfinite(ac[i,:])) | (ac[i,:]==0)
        if np.count_nonzero(id_notvalid)==0: valid_count+=1
        else: ids[i]=False
print valid_count

ac = ac[ids,:]
ac = ac/ac[:,Vloc][:,np.newaxis]
acme = np.median(ac,axis=0)
acup=np.percentile(ac,84,axis=0)
aclo=np.percentile(ac,16,axis=0)
ax.plot(lamb/1e4,acme,c='seagreen',lw=4,alpha=0.6,label=r'$\rm \log{M_{\ast}\in[7.5,8.5]M_{\odot}}$')
ax.fill_between(lamb/1e4,y1=aclo,y2=acup,alpha=0.1,color='seagreen')

print fit(acme), fit(acup), fit(aclo)

lamb=np.linspace(1000,10000,1000)

curve_mw=0.0*lamb
for i in range(len(lamb)):
	curve_mw[i]=Curve_MW(lamb[i])
ax.plot(lamb/1e4,curve_mw,'--',dashes=(25,15),c='gray')

curve_calzetti=0.0*lamb
for i in range(len(lamb)):
	curve_calzetti[i]=Curve_Calzetti(lamb[i])
ax.plot(lamb/1e4,curve_calzetti,'--',dashes=(25,15),c='k')

curve_kriek=0.0*lamb
for i in range(len(lamb)):
	curve_kriek[i]=Curve_Kriek(lamb[i],0)
ax.plot(lamb/1e4,curve_kriek,'--',dashes=(25,15),c='chocolate')

prop = matplotlib.font_manager.FontProperties(size=22.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1)
ax.set_xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$A_{\lambda}/A_{\rm V}$',fontsize=40,labelpad=5)
ax.set_xlim(min(lamb)/1e4,max(lamb)/1e4)
ax.set_ylim(0.2,3.5)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig("../figs/attenuation_curves_mass.pdf",format='pdf')

