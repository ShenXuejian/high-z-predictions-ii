import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.font_manager
from curves import *
import tables
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=2,markersize=20)
matplotlib.rc('axes', linewidth=2)

fig=plt.figure(figsize=(15,10))
ax  = fig.add_axes([0.11,0.12,0.79,0.83])

lamb=np.genfromtxt("waves.dat")[1:]*1e4
Vloc = np.arange(0,len(lamb),dtype=np.int32)[lamb>5510][0]
with tables.open_file("outpath/TNG50-1/attenuation_curve_33_b.hdf5") as f:
	ac = f.root.attenuation_curve[:].copy()

print ac.shape
valid_count = 0
ids = (np.ones(ac.shape[0])!=0)
for i in range(ac.shape[0]):
	id_notvalid = np.invert(np.isfinite(ac[i,:])) | (ac[i,:]==0)
	if np.count_nonzero(id_notvalid)==0: valid_count+=1
	else: ids[i]=False
print valid_count

ac = ac[ids,:]
ac = ac/ac[:,Vloc][:,np.newaxis]
acme = np.median(ac,axis=0)
acup=np.percentile(ac,84,axis=0)
aclo=np.percentile(ac,16,axis=0)
ax.plot(lamb/1e4,acme,c='royalblue',lw=4,alpha=0.8,label=r'$\rm TNG50:$ $\rm optically$ $\rm blue$ ($(g-r)_{\rm i}<0.2$)'+'\n'+r'$\rm Draine+$ $\rm 2007$ $\rm dust$ $\rm model$')
ax.fill_between(lamb/1e4,y1=aclo,y2=acup,alpha=0.2,color='royalblue')

with tables.open_file("outpath/TNG50-1/attenuation_curve_33_r.hdf5") as f:
        ac = f.root.attenuation_curve[:].copy()

print ac.shape
valid_count = 0
ids = (np.ones(ac.shape[0])!=0)
for i in range(ac.shape[0]):
        id_notvalid = np.invert(np.isfinite(ac[i,:])) | (ac[i,:]==0)
        if np.count_nonzero(id_notvalid)==0: valid_count+=1
        else: ids[i]=False
print valid_count

ac = ac[ids,:]
ac = ac/ac[:,Vloc][:,np.newaxis]
acme = np.median(ac,axis=0)
acup=np.percentile(ac,84,axis=0)
aclo=np.percentile(ac,16,axis=0)
ax.plot(lamb/1e4,acme,c='crimson',lw=4,alpha=0.8,label=r'$\rm TNG50:$ $\rm optically$ $\rm red$ ($(g-r)_{\rm i}>0.2$)')
ax.fill_between(lamb/1e4,y1=aclo,y2=acup,alpha=0.2,color='crimson')

lamb=np.linspace(1000,10000,1000)

curve_mw=0.0*lamb
for i in range(len(lamb)):
	curve_mw[i]=Curve_MW(lamb[i])
ax.plot(lamb/1e4,curve_mw,'--',dashes=(25,15),lw=3,c='darkorchid')

curve_calzetti=0.0*lamb
for i in range(len(lamb)):
	curve_calzetti[i]=Curve_Calzetti(lamb[i])
ax.plot(lamb/1e4,curve_calzetti,'--',dashes=(25,15),lw=3,c='k')

curve_kriek=0.0*lamb
for i in range(len(lamb)):
	curve_kriek[i]=Curve_Kriek(lamb[i],0)
ax.plot(lamb/1e4,curve_kriek,'--',dashes=(25,15),lw=3,c='chocolate')

ax.text(0.87, 0.73, r'$M_{\rm UV}<-17$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')
ax.text(0.75, 0.66, r'$7.5 < \log{(M_{\ast}/{\rm M}_{\odot})} \lesssim 10-11$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')

ax.text(0.10, 0.08, r'$\rm z=2$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

prop = matplotlib.font_manager.FontProperties(size=27.0)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=1,ncol=1,frameon=False)
ax.set_xlabel(r'$\lambda$ $[\rm{\mu m}]$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$A_{\lambda}/A_{\rm V}$',fontsize=40,labelpad=5)
ax.set_xlim(min(lamb)/1e4,max(lamb)/1e4)
ax.set_ylim(0.2,3.5)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig("../figs/attenuation_curves_rb.pdf",format='pdf')

