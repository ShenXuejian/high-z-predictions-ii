import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.font_manager
matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(figsize=(15,10))
ax  = fig.add_axes([0.11,0.12,0.79,0.83])

re = np.array([[10.,0.35978617,0.18886944,0.47371982,0.5],
		[9.,0.12977464,-0.09369802,0.30007917,0.5],
		[8.,-0.07951192,-0.35444026,0.18052739,0.5]])

yerr=(re[:,1]-re[:,2],re[:,3]-re[:,1])
ax.errorbar( re[:,0], re[:,1], yerr=yerr, xerr=re[:,4], linestyle='none',marker='o',c='royalblue',mec='royalblue',ms=15,capsize=9,capthick=4,label=r'$\rm TNG50$')

data = np.genfromtxt("JAM2019.dat",names=True)
ax.errorbar( data['logM'], data['delta'], yerr=(data['delta']-data['lo'],data['up']-data['delta']),xerr=(data["logM"]-data["left"],data["right"]-data["logM"]), linestyle='none',marker='o',c='crimson',mec='crimson',ms=15,capsize=0,capthick=4,label=r'$\rm \'{A}lvarez-M\'{a}rquez+$ $\rm 2019$')
'''
xs = np.linspace(9.,11., 100)
ys = -0.38 + 0.29*(xs-10.)
ax.plot(xs, ys, '-' , linestyle='--',dashes=(25,15),c='seagreen',mec='seagreen',label=r'$\rm Salim+$ $\rm 2018$ ($\rm local$ $\rm galaxies$)')

xs = np.linspace(9.,11., 100)
ys = -0.45 + 0.19*(xs-10.)
ax.plot(xs, ys, '-' , linestyle='--',dashes=(25,15),c='seagreen',mec='seagreen',label=r'$\rm Salim+$ $\rm 2018$ ($\rm high-z$ $\rm analogs$)')
'''
ax.text(0.86, 0.24, r'$ (g-r)_{\rm i} < 0.2$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')
ax.text(0.87, 0.17, r'$M_{\rm UV}<-17$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')

ax.text(0.90, 0.08, r'${\rm z='+str(3)+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

prop = matplotlib.font_manager.FontProperties(size=27)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,loc=2,ncol=1,frameon=False)
ax.set_xlabel(r'$\log{(M_{\ast}[{\rm M}_{\odot}])}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\delta$',fontsize=40,labelpad=5)
ax.set_xlim(7.45,11.5)
ax.set_ylim(-0.4,0.55)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig("../figs/slope_mass.pdf",format='pdf')

