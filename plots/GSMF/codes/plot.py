import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = 'TNG300-1'
from data import *
import matplotlib.font_manager
from mstarcorr_module import *
from scipy.optimize import curve_fit

matplotlib.style.use('classic')

smass_bins=np.linspace(7,12.5,23)
cx=(smass_bins[1:]+smass_bins[:-1])/2.
lenbin=cx[5]-cx[4]

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def mstar_sch(M,phi_s,M_s,alpha): #M in log10; phi_s in log10
        return np.log10( np.log(10.)*(10**phi_s)*np.power(10.,(M-M_s)*(alpha+1.))*np.exp(-np.power(10.,M-M_s)) )

def plt_obdata(fname,papername,color,label=True):
        obdata=np.genfromtxt("../../obdata/"+fname,names=True, comments='#')
        x_ob=obdata['m']
        phi=obdata['phi']
        id1=phi>0
        id2= (phi<=0)
        y_ob=0.0*x_ob
        y_ob[id1]=np.log10(phi[id1])
        y_ob[id2]=phi[id2]
        uperr=0.0*x_ob
        uperr[id1]=np.log10(phi[id1]+obdata['uperr'][id1])-y_ob[id1]
        uperr[id2]=obdata['uperr'][id2]
        low=phi-obdata['lowerr']
        low[low<=0]=1e-10
        lowerr=0.0*x_ob
        lowerr[id1]=-np.log10(low[id1])+y_ob[id1]
        lowerr[id2]=obdata['lowerr'][id2]
        if label==True:
                ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5,label=papername)
        else: ax.errorbar(x_ob,y_ob,yerr=(lowerr,uperr),c=color,lw=3,linestyle='',marker='o',markersize=9,capsize=4.5)

eff={
	'4'  :{'TNG50-1':[6,9.00],'TNG100-1':[8.5,10.0],'TNG300-1':[9.30,14]},
	'6'  :{'TNG50-1':[6,9.50],'TNG100-1':[8.5,10.0],'TNG300-1':[9.30,14]},
	'8'  :{'TNG50-1':[6,10.0],'TNG100-1':[8.5,10.0],'TNG300-1':[9.80,14]},
	'11' :{'TNG50-1':[6,10.0],'TNG100-1':[9.0,10.4],'TNG300-1':[9.80,14]},
        '13' :{'TNG50-1':[6,10.0],'TNG100-1':[9.0,10.4],'TNG300-1':[10.2,14]},
        '17' :{'TNG50-1':[6,10.5],'TNG100-1':[9.2,11.0],'TNG300-1':[10.2,14]},
        '21' :{'TNG50-1':[6,11.0],'TNG100-1':[9.4,11.4],'TNG300-1':[10.4,14]},
	'25' :{'TNG50-1':[6,11.0],'TNG100-1':[9.6,11.4],'TNG300-1':[10.4,14]},
	'33' :{'TNG50-1':[6,11.0],'TNG100-1':[9.6,11.6],'TNG300-1':[10.6,14]}}
# a general combine function
def combine(v50,v100,v300,n50,n100,n300,snapnum):
        v50[np.invert(np.isfinite(v50))]=0
        n50[np.invert(np.isfinite(v50))]=1e-37
        v100[np.invert(np.isfinite(v100))]=0
        n100[np.invert(np.isfinite(v100))]=1e-37
        v300[np.invert(np.isfinite(v300))]=0
        n300[np.invert(np.isfinite(v300))]=1e-37
        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
        binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][1])
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][1]) # (n300>5)
        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def plt_data(snapnum,redshift,label=False):
	cxfit=np.linspace(6.5,13,1000)
	fits=np.genfromtxt("schechter_fits.dat",names=True)
	id=fits["snap"]==snapnum
        logpfit=mstar_sch(cxfit,fits["LogPhis"][id],fits["LogMs"][id],fits["alpha"][id])
        if label==True: ax.plot(cxfit,logpfit,'-',lw=7,c='cyan',alpha=0.5,label=r'$\rm Schechter$ $\rm fit$')
        else: ax.plot(cxfit,logpfit,'-',lw=7,c='cyan',alpha=0.5)

	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
	f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
	mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
	mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
	mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
	f.close()

	fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	f.close()
	count50,b=np.histogram(np.log10(mstar),bins=smass_bins)
	phi50= np.log10(count50/lenbin/((35./hubble)**3))
	if snapnum==8: ax.plot(cx,phi50,'-',c='royalblue',alpha=0.6,label=r'$\rm TNG50-1$')
	else: ax.plot(cx,phi50,'-',c='royalblue',alpha=0.6)	

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	subids=f.root.subids[:].copy()
	halomass=mhalo['TNG100-1'][subids]
	mstar=mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)
	f.close()
	count100,b=np.histogram(np.log10(mstar),bins=smass_bins)
	phi100= np.log10(count100/lenbin/((75./hubble)**3))
	if snapnum==8: ax.plot(cx,phi100,'-',c='crimson',alpha=0.4,label=r'$\rm TNG100-1$ ($\rm corrected$)')
	else: ax.plot(cx,phi100,'-',c='crimson',alpha=0.4)	

	fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	subids=f.root.subids[:].copy()
	halomass=mhalo['TNG300-1'][subids]
	mstar=mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)
	f.close()
	count300,b=np.histogram(np.log10(mstar),bins=smass_bins)
	phi300= np.log10(count300/lenbin/((205./hubble)**3))
	if snapnum==8: ax.plot(cx,phi300,'-',c='seagreen',alpha=0.4,label=r'$\rm TNG300-1$ ($\rm corrected$)')
	else: ax.plot(cx,phi300,'-',c='seagreen',alpha=0.4)

	phi_combined = combine(phi50,phi100,phi300,count50,count100,count300,snapnum)
	ax.plot(cx,phi_combined,'k--',dashes=(25,15),lw=5,label=r'$\rm combined$')
	np.savetxt("./combined/combined_SMF_"+str(snapnum)+".dat",np.c_[cx,phi_combined])

	phi_limit= np.log10( 10./lenbin/(boxlength/1000./hubble)**3 )
	id= np.isfinite(phi_combined) & (phi_combined>phi_limit)	
	args,_=curve_fit(mstar_sch,cx[id],phi_combined[id],p0=np.array([-5,10.7,-1.90]))
	print snapnum,':',args

fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,10))
row=3
col=3

x0,y0,width,height,wspace,hspace=0.05,0.05,0.3,0.3,0,0
for i in range(9):
	nh,nw=2-i/3,i%3
        ax=fig.add_axes([x0+nw*(width+wspace),y0+nh*(height+hspace),width,height])

	if i==0:
		plt_data(Snaps[i],Zs[i],label=True)
	else: 
		plt_data(Snaps[i],Zs[i])
	if Snaps[i]==21:
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_dun14.dat',r'$\rm Duncan+$ $\rm 2014$','black')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ste17.dat',r'$\rm Stefanon+$ $\rm 2017$','gray')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_song16.dat',r'$\rm Song+$ $\rm 2016$','saddlebrown')
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_gonz11.dat',r'$\rm Gonzalez+$ $\rm 2011$','navy')
	elif Snaps[i]==17:
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ste17.dat','','gray')
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_song16.dat','','saddlebrown')
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_gonz11.dat','','navy')
	elif Snaps[i]==13:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ste17.dat','','gray')
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_song16.dat','','saddlebrown')
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_gonz11.dat','','navy')
	elif Snaps[i]==11:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_ste17.dat','','gray')
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_song16.dat','','saddlebrown')
                plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_gonz11.dat','','navy')
	elif Snaps[i]==8:
		plt_obdata('obdata'+str(Snaps[i]).zfill(3)+'_song16.dat','','saddlebrown')

	prop = matplotlib.font_manager.FontProperties(size=30)
	if Snaps[i] in [21]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)
	elif i==0: 	
		ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

	ax.text(0.85, 0.92, r'${\rm z='+str(Zs[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_xlim(6.75,12.75)
	ax.set_ylim(-6.3,-0.75)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	if (i/row==col-1):
		ax.set_xlabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
	else: ax.set_xticklabels([])
	if i%row==0:
		ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
	else:
		ax.set_yticklabels([])
#plt.show()
plt.savefig(figpath+'GSMF.pdf',fmt='pdf')
