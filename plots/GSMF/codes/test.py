import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = 'TNG300-1'
from data import *
import matplotlib.font_manager
from mstarcorr_module import *
from scipy.optimize import curve_fit

matplotlib.style.use('classic')

smass_bins=np.linspace(7,12.5,23)
cx=(smass_bins[1:]+smass_bins[:-1])/2.
lenbin=cx[5]-cx[4]

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def plt_data(snapnum,redshift,label=False):
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
	f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
	mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
	mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
	mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
	f.close()

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	subids=f.root.subids[:].copy()
	halomass=mhalo['TNG100-1'][subids]
	mstar=mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)
	f.close()
	count100,b=np.histogram(np.log10(mstar),bins=smass_bins)
	phi100= np.log10(count100/lenbin/((75./hubble)**3))
	if snapnum==8: ax.plot(cx,phi100,'-',c='crimson',label=r'$\rm TNG100-1$ ($\rm corrected$)')
	else: ax.plot(cx,phi100,'-',c='crimson')	

	fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	subids=f.root.subids[:].copy()
	halomass=mhalo['TNG300-1'][subids]
	mstar=mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)
	f.close()
	count300,b=np.histogram(np.log10(mstar),bins=smass_bins)
	phi300= np.log10(count300/lenbin/((205./hubble)**3))
	if snapnum==8: ax.plot(cx,phi300,'-',c='seagreen',label=r'$\rm TNG300-1$ ($\rm corrected$)')
	else: ax.plot(cx,phi300,'-',c='seagreen')

fig=plt.figure(figsize = (15,10))
ax=fig.add_axes([0.13,0.11,0.79,0.83])

plt_data(25,3,label=True)

ax.set_xlim(8.75,12.75)
ax.set_ylim(-6.3,-0.75)
ax.axis('on')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
ax.set_xlabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
plt.show()
