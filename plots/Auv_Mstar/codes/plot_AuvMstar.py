import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager

from mstarcorr_module import *
from magcorr_module import *
from magcorr_module_nmap import *
from sfrcorr_module import *

bins=np.linspace(7,12.5,23)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
	'13' :{'TNG50-1':[6.0,10.0],'TNG100-1':[9.0,10.5],'TNG300-1':[10.1,13]},
	'17' :{'TNG50-1':[6.0,10.0],'TNG100-1':[8.8,10.5],'TNG300-1':[10.1,13]},
	'21' :{'TNG50-1':[6.0,10.0],'TNG100-1':[8.8,10.8],'TNG300-1':[10.2,13]},
	'25' :{'TNG50-1':[6.0,10.0],'TNG100-1':[8.8,10.8],'TNG300-1':[10.2,13]},
	'33' :{'TNG50-1':[6.0,10.4],'TNG100-1':[9.0,11.2],'TNG300-1':[10.7,13]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter>=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter>=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter>=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter<=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	#return (np.percentile(x[np.isfinite(x)],70)-np.percentile(x[np.isfinite(x)],30))/2.
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def plt_data(snapnum,redshift):
	model = 'C'

	ssfrcut = -9.0
	
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()

	if snapnum<=13: 
		the_path=outpath_C_nmap
	else: 
		the_path=outpath_C

	fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	f.close()

	fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
	f.close()				

	fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        ssfr = np.log10(sfr/mstar)

	halomass = mhalo['TNG50-1'][subids]
	bestfit=best_fits[model][str(snapnum)]+1	
	Auv = mags[:,bestfit] - mags[:,0]

	valid = np.isfinite(mstar) & np.isfinite(Auv) & (ssfr>ssfrcut) & (mags[:,bestfit]<-17) 
	Auv[Auv<0]=0
	num50,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic='count', bins=bins)
	med50,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic='median', bins=bins)
	sigma50,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic=std, bins=bins)
		
	#id=(num50>10)
	#ax.plot(cx[id],med50[id],linestyle='-',lw=4 ,c='royalblue')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')
		
	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	halomass=mhalo['TNG100-1'][subids]
	if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
	else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
	f.close()

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	f.close()

	halomass=mhalo['TNG100-1'][subids]
	mstar = mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)

	fname=sfrPath+'TNG100-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG100-1',snapnum)
        ssfr = np.log10(sfr/mstar)

	Auv = mags[:,bestfit] - mags[:,0]
	bestfit=best_fits[model][str(snapnum)]+1
	valid = np.isfinite(mstar) & np.isfinite(Auv) & (ssfr>ssfrcut) & (mags[:,bestfit]<-17)
	Auv[Auv<0]=0

	num100,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic='count', bins=bins)
	med100,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic='median', bins=bins)
	sigma100,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic=std, bins=bins)
	#id=(num100>10)
	#ax.plot(cx[id],med100[id],linestyle='-',lw=4 ,c='crimson')
	#ax.errorbar(cx[id],med100[id],yerr=sigma100[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='crimson',capsize=10,c='crimson',alpha=0.6)
		
	fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	halomass=mhalo['TNG300-1'][subids]
	if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG300-1',snapnum,model)
	else: mags=mags+get_corr(halomass,'TNG300-1',snapnum,model)
	f.close()

	fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	f.close()
	
	halomass=mhalo['TNG300-1'][subids]
	mstar = mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)

	fname=sfrPath+'TNG300-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG300-1',snapnum)
        ssfr = np.log10(sfr/mstar)

	Auv = mags[:,bestfit] - mags[:,0]
	bestfit=best_fits[model][str(snapnum)]+1
	valid = np.isfinite(mstar) & np.isfinite(Auv) & (ssfr>ssfrcut) & (mags[:,bestfit]<-17)
	Auv[Auv<0]=0

	num300,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic='count', bins=bins)
	med300,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic='median', bins=bins)
	sigma300,b,_=st.binned_statistic(np.log10(mstar[valid]), Auv[valid] , statistic=std, bins=bins)

	#id=(num300>10)
	#ax.plot(cx[id],med300[id],linestyle='-',lw=4 ,c='seagreen')
	#ax.errorbar(cx[id],med300[id],yerr=sigma300[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='seagreen',capsize=10,c='seagreen',alpha=0.4)
	def convert(logIRX,B=1.71):
                        return np.log10(10**logIRX/B+1)*2.5
        def convert2(logIRX):
                IRX = logIRX
                return -0.0333*IRX**3 + 0.3522*IRX**2 + 1.1960*IRX + 0.4967

	if snapnum in [33,25]:
		data=np.genfromtxt("obdata/Pannella2015.dat",names=True)
		select=data["snap"]==snapnum
		ax.errorbar(data["Mstar"][select],data["Auv"][select],yerr=(data["Auv"][select]-data["lo"][select],data["up"][select]-data["Auv"][select]),color='crimson', mec='crimson', lw=4, capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Pannella+$ $\rm 2015$')

	if snapnum==25:
		data=np.genfromtxt("obdata/JAM2015_z3.dat",names=True)
		ax.errorbar(data["Mstar"],data["Auv"],yerr=(data["Auv"]-data["lo"],data["up"]-data["Auv"]),xerr=(data["Mstar"]-data["left"],data["right"]-data["Mstar"]), color='seagreen', mec='seagreen', lw=4, capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm \'{A}lvarez\,M\'{a}rquez+$ $\rm 2016$')

		data=np.genfromtxt("obdata/Bourne2017.dat",names=True)
                ax.errorbar(data["logM"],convert(data["logIRX"],B=1.68),yerr=(convert(data["logIRX"],B=1.68)-convert(data["lo"],B=1.68),convert(data["up"],B=1.68)-convert(data["logIRX"],B=1.68)), color='olive', mec='olive', lw=4, capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Bourne+$ $\rm 2017$')

		data=np.genfromtxt("obdata/Mclure2018_z2-3.dat",names=True)
		select=data["type"]==2
		ax.errorbar(data["Mstar"][select],data["Auv"][select],yerr=(data["Auv"][select]-data["lo"][select],data["up"][select]-data["Auv"][select]),xerr=(data["Mstar"][select]-data["left"][select],data["right"][select]-data["Mstar"][select]), color='darkorchid', mec='darkorchid', lw=4, capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Mclure+$ $\rm 2018$ ($\rm z=2.5$)')
		select=data["type"]==1
		ax.errorbar(data["Mstar"][select],data["Auv"][select],yerr=(data["Auv"][select]-data["lo"][select],data["up"][select]-data["Auv"][select]), color='darkorchid', mec='darkorchid', lw=4, capsize=0, marker='o',markersize=15, linestyle='none')
		select=data["type"]==0
		ax.errorbar(data["Mstar"][select],data["Auv"][select],xerr=(data["Mstar"][select]-data["left"][select],data["right"][select]-data["Mstar"][select]), color='darkorchid', mec='darkorchid', lw=4, capsize=0, marker='o',markersize=15, linestyle='none')
		ax.errorbar(data["Mstar"][select],data["Auv"][select],yerr=0.3, uplims=True, color='darkorchid', mec='darkorchid', capsize=9, marker='o',markersize=15, linestyle='none')

        	data=np.genfromtxt("obdata/Koprowski2018_z3.dat",names=True)
        	select=data["type"]==1
        	ax.errorbar(data["Mstar"][select],convert(data["logIRX"][select],B=1.56),yerr=(convert(data["logIRX"][select],B=1.56)-convert(data["lo"][select],B=1.56),convert(data["up"][select],B=1.56)-convert(data["logIRX"][select],B=1.56)),xerr=(data["Mstar"][select]-data["left"][select],data["right"][select]-data["Mstar"][select]), color='chocolate', mec='chocolate', capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Koprowski+$ $\rm 2018$ ($\rm z=3.35$)')
        	select=data["type"]==0
        	ax.errorbar(data["Mstar"][select],convert(data["logIRX"][select],B=1.56),xerr=(data["Mstar"][select]-data["left"][select],data["right"][select]-data["Mstar"][select]), color='chocolate', mec='chocolate', capsize=0, marker='o',markersize=15, linestyle='none')
        	ax.errorbar(data["Mstar"][select],convert(data["logIRX"][select],B=1.56),yerr=0.3,uplims=True, color='chocolate', mec='chocolate', capsize=9, marker='o',markersize=15, linestyle='none')

	if snapnum==33:
		data=np.genfromtxt("obdata/Heinis2014_z2.dat",names=True)
                ax.errorbar(data["logM"],convert2(data["logIRX"]),yerr=(convert2(data["logIRX"])-convert2(data["lo"]),convert2(data["up"])-convert2(data["logIRX"])),xerr=(data["logM"]-data["left"],data["right"]-data["logM"]), color='k', mec='k', capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Heinis+$ $\rm 2014$')
	if snapnum==25:
		data=np.genfromtxt("obdata/Heinis2014_z3.dat",names=True)
		ax.errorbar(data["logM"],convert2(data["logIRX"]),yerr=(convert2(data["logIRX"])-convert2(data["lo"]),convert2(data["up"])-convert2(data["logIRX"])),xerr=(data["logM"]-data["left"],data["right"]-data["logM"]), color='k', mec='k', capsize=0, marker='o',markersize=15, linestyle='none')	
	if snapnum==21:
		data=np.genfromtxt("obdata/Heinis2014_z4.dat",names=True)
                ax.errorbar(data["logM"],convert2(data["logIRX"]),yerr=(convert2(data["logIRX"])-convert2(data["lo"]),convert2(data["up"])-convert2(data["logIRX"])),xerr=(data["logM"]-data["left"],data["right"]-data["logM"]), color='k', mec='k', capsize=0, marker='o',markersize=15, linestyle='none')
		

	if snapnum==33:
		data=np.genfromtxt("obdata/Reddy2018.dat",names=True)
                select=data["type"]==1
                ax.errorbar(data["logM"][select],convert(data["logIRX"][select]), color='deeppink', mec='deeppink', capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Reddy+$ $\rm 2018$')
                select=data["type"]==0
                ax.errorbar(data["logM"][select],convert(data["logIRX"][select]),xerr=0.1, color='deeppink', mec='deeppink', capsize=0, marker='o',markersize=15, linestyle='none')
                ax.errorbar(data["logM"][select],convert(data["logIRX"][select]),yerr=0.3,uplims=True, color='deeppink', mec='deeppink', capsize=9, marker='o',markersize=15, linestyle='none')

	######################################
	if snapnum in [33,25,21]:
		x_fit=np.linspace(6,15,100)
		ax.plot( x_fit, x_fit*1.6-13.5, '--', dashes=(25,15), c='cyan', label=r'$\rm P15$ $\rm fit$' ,alpha=0.5)
	if snapnum==25:
		x_fit=np.linspace(6,15,100)
                ax.plot( x_fit, convert((x_fit-10.35)*0.84+1.17, B=1.68), '--', dashes=(25,15), c='gray', label=r'$\rm \'{A}M16$ $\rm fit$', alpha=0.3)

		x_fit=np.linspace(6,15,100)
                ax.plot( x_fit, convert((x_fit-10)*0.87+0.98, B=1.56), '--', dashes=(25,15), c='navy', label=r'$\rm K18$ $\rm fit$' , alpha=0.3)

		x_fit=np.linspace(6,15,100)
		X=x_fit-10.
                ax.plot( x_fit, 2.293+1.160*X+0.256*X**2+0.209*X**3, '--', dashes=(25,15), c='k', label=r'$\rm M18$ $\rm fit$' , alpha=0.3)

	######################################
	med = combine(med50,med100,med300,num50,num100,num300,snapnum)
	sigma = combine(sigma50,sigma100,sigma300,num50,num100,num300,snapnum)
	if snapnum==33: ax.errorbar(cx,med,yerr=sigma,linestyle='-',lw=4,marker='o',markersize=15,markeredgecolor='royalblue',capsize=10,capthick=4,c='royalblue',label=r'$\rm TNG$')
	else: ax.errorbar(cx,med,yerr=sigma,linestyle='-',lw=4,marker='o',markersize=15,markeredgecolor='royalblue',capsize=10,capthick=4,c='royalblue')

redshifts=[2,3,4,6]
snapnums=[33,25,21,13]

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(4):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i])
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=24)
	if i in [0,1]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=2,frameon=False)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.77, 0.16, r'$\log{({\rm sSFR}/{\rm yr}^{-1})}>-9.0$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')
        ax.text(0.87, 0.23, r'$M_{\rm UV}<-17$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')

	ax.text(0.90, 0.08, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

	ax.set_xlim(7.2,12.3)
	ax.set_ylim(0,3.8)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_ylabel(r'$A_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
	ax.set_xlabel(r'$\log{(M_{\ast}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'Auv-Mstar-'+str(i)+'.pdf',fmt='pdf')

