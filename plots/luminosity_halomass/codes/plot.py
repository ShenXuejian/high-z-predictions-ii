import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager
from magcorr_module import *
from magcorr_module_nmap import *

bins=np.linspace(-24,-16, 24)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff_dust={
	'8'  :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.7,-22.0],'TNG300-1':[-21.2,-24]},
	'11' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.7,-22.0],'TNG300-1':[-21.2,-24]},
	'13' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.7,-22.0],'TNG300-1':[-21.6,-24]},
	'17' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-18.9,-22.0],'TNG300-1':[-21.3,-24]},
	'21' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-21.2,-24]}
}
eff_int={
	'8'  :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.7,-22.0],'TNG300-1':[-20.8,-24]},
	'11' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.8,-22.0],'TNG300-1':[-21.6,-24]},
        '13' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-20.3,-24.0],'TNG300-1':[-23.6,-24]},
        '17' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.4,-24.0],'TNG300-1':[-22.6,-24]},
        '21' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.0,-24.0],'TNG300-1':[-22.2,-24]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum,dust=True):
	if dust==True: eff=eff_dust
	else: eff=eff_int

	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter<=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter<=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter<=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def plt_data(snapnum,redshift,model):
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()

	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C
	
	if 1==1:
		fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
                f=tables.open_file(fname)
                subids=f.root.subids[:].copy()
                mags= f.root.band_magnitudes[:,0,:].copy()
                f.close()

        	halomass = mhalo["TNG50-1"][subids]

		bestfit=best_fits[model][str(snapnum)]+1	

		num50,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic='count', bins=bins)
		med50,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic='median', bins=bins)
		sigma50,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic=std, bins=bins)
		
		#id=(num50>10)
		#ax.plot(cx[id],med50[id],linestyle='-',lw=4 ,c='royalblue')
		#else: ax.errorbar(cx[id],med[id],yerr=sigma[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')
		
		num50i,b,_=st.binned_statistic(mags[:,0], np.log10(halomass) , statistic='count', bins=bins)
		med50i,b,_=st.binned_statistic(mags[:,0], np.log10(halomass) , statistic='median', bins=bins)
		#id=(num50i>10)
                #ax.plot(cx[id],med50i[id],':',lw=6,c='royalblue',alpha=0.6)
			
		fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
                f=tables.open_file(fname)
                subids=f.root.subids[:].copy()
                mags= f.root.band_magnitudes[:,0,:].copy()
		halomass=mhalo['TNG100-1'][subids]
		if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
		else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
		f.close()

                num100,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic='count', bins=bins)
                med100,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic='median', bins=bins)
               	sigma100,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic=std, bins=bins)

		#id=(num100>10)
		#ax.plot(cx[id],med100[id],linestyle='-',lw=4,c='crimson')
                #else: ax.errorbar(cx[id],med[id],yerr=sigma[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='crimson',capsize=10,c='crimson')

		num100i,b,_=st.binned_statistic(mags[:,0], np.log10(halomass) , statistic='count', bins=bins)
                med100i,b,_=st.binned_statistic(mags[:,0], np.log10(halomass) , statistic='median', bins=bins)
                #id=(num100i>10)
                #ax.plot(cx[id],med100i[id],':',lw=6,c='crimson',alpha=0.6)

		fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
                f=tables.open_file(fname)
                subids=f.root.subids[:].copy()
                mags= f.root.band_magnitudes[:,0,:].copy()
                halomass=mhalo['TNG300-1'][subids]
                if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG300-1',snapnum,model)
                else: mags=mags+get_corr(halomass,'TNG300-1',snapnum,model)
		f.close()

                num300,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic='count', bins=bins)
                med300,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic='median', bins=bins)
		sigma300,b,_=st.binned_statistic(mags[:,bestfit], np.log10(halomass) , statistic=std, bins=bins)
                #id=(num300>10)
		#ax.plot(cx[id],med300[id],linestyle='-',lw=4,c='seagreen')
                #else: ax.errorbar(cx[id],med[id],yerr=sigma[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='seagreen',capsize=10,c='seagreen')

                num300i,b,_=st.binned_statistic(mags[:,0], np.log10(halomass) , statistic='count', bins=bins)
                med300i,b,_=st.binned_statistic(mags[:,0], np.log10(halomass) , statistic='median', bins=bins)
                #id=(num300i>10)
                #ax.plot(cx[id],med300i[id],':',lw=6,c='seagreen',alpha=0.6)

		if snapnum==21:
			#data = np.genfromtxt("../data_compare/mason2015_z6.dat",names=['y','x'])
                        #ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='seagreen',label=r'$\rm Mason+$ $\rm 2015$')

			data = np.genfromtxt("../data_compare/tacchella2013_z4.dat",names=['y','x'])
			ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='crimson',label=r'$\rm Tacchella+$ $\rm 2013$', alpha=0.7)

			data = np.genfromtxt("../data_compare/tacchella2018/Tacchella_table_MuvMh_z4.txt",names=True)
			ax.errorbar(data['Muv'],data['log_Mh'],yerr=(data['log_Mh']-data['log_Mh_low'],data['log_Mh_up']-data['log_Mh']),c='darkorchid',mec='darkorchid',marker='o',ms=15,capsize=0,label=r'$\rm Tacchella+$ $\rm 2018$', alpha=0.7)

			data = np.genfromtxt("../data_compare/endsley2019/z4.dat",names=['x','y'])
                        ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='cyan',label=r'$\rm Endsley+$ $\rm 2019$',alpha=0.9)

		if snapnum==17:
			data = np.genfromtxt("../data_compare/tacchella2018/Tacchella_table_MuvMh_z5.txt",names=True)
                        ax.errorbar(data['Muv'],data['log_Mh'],yerr=(data['log_Mh']-data['log_Mh_low'],data['log_Mh_up']-data['log_Mh']),c='darkorchid',mec='darkorchid',marker='o',ms=15,capsize=0,alpha=0.7, label=r'$\rm Tacchella+$ $\rm 2018$')

		if snapnum==13:
                        #data = np.genfromtxt("../data_compare/mason2015_z6.dat",names=['y','x'])
                        #ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='seagreen',label=r'$\rm Mason+$ $\rm 2015$')

			data = np.genfromtxt("../data_compare/tacchella2018/Tacchella_table_MuvMh_z6.txt",names=True)
                        ax.errorbar(data['Muv'],data['log_Mh'],yerr=(data['log_Mh']-data['log_Mh_low'],data['log_Mh_up']-data['log_Mh']),c='darkorchid',mec='darkorchid',marker='o',ms=15,capsize=0,alpha=0.7, label=r'$\rm Tacchella+$ $\rm 2018$')

			data = np.genfromtxt("../data_compare/endsley2019/z6.dat",names=['x','y'])
                        ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='cyan',alpha=0.9,label=r'$\rm Endsley+$ $\rm 2019$')

			data = np.genfromtxt("../data_compare/trac2015_z6.dat",names=['y','x'])
                        ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='seagreen',label=r'$\rm Trac+$ $\rm 2015$',alpha=0.7)

		if snapnum==8:
                        data = np.genfromtxt("../data_compare/tacchella2013_z8.dat",names=['y','x'])
                        ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='crimson',alpha=0.7,label=r'$\rm Tacchella+$ $\rm 2013$')

			x_ob = np.linspace(-24,-16,100)
			y_ob = np.log10( 278.*10**(-0.44*x_ob) )
			ax.plot(x_ob, y_ob, '--', dashes=(25,15), c='chocolate', label=r'$\rm Shimizu+$ $\rm 2014$',alpha=0.7)

			data = np.genfromtxt("../data_compare/tacchella2018/Tacchella_table_MuvMh_z8.txt",names=True)
                        ax.errorbar(data['Muv'],data['log_Mh'],yerr=(data['log_Mh']-data['log_Mh_low'],data['log_Mh_up']-data['log_Mh']),c='darkorchid',mec='darkorchid',marker='o',ms=15,capsize=0,alpha=0.7,label=r'$\rm Tacchella+$ $\rm 2018$')

			data = np.genfromtxt("../data_compare/endsley2019/z8.dat",names=['x','y'])
                        ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='cyan',alpha=0.9,label=r'$\rm Endsley+$ $\rm 2019$')

			data = np.genfromtxt("../data_compare/trac2015_z8.dat",names=['y','x'])
                        ax.plot(data['x'],data['y'],'--',dashes=(25,15),c='seagreen',alpha=0.7,label=r'$\rm Trac+$ $\rm 2015$')

		med = combine(med50,med100,med300,num50,num100,num300,snapnum)
		sigma = combine(sigma50,sigma100,sigma300,num50,num100,num300,snapnum)
		if snapnum==21: ax.errorbar(cx,med,yerr=sigma,linestyle='-',lw=4,marker='o',markersize=15,mec='royalblue',capsize=9,capthick=4,c='royalblue',label=r'$\rm TNG:$ $\rm with$ $\rm all$ $\rm dust$')
		else: ax.errorbar(cx,med,yerr=sigma,linestyle='-',lw=4,marker='o',markersize=15,mec='royalblue',capsize=9,capthick=4,c='royalblue')

		medi = combine(med50i,med100i,med300i,num50i,num100i,num300i,snapnum,dust=False)
		#sigmai = combine(sigma50,sigma100,sigma300,num50,num100,num300,snapnum)
		if snapnum==21: ax.plot(cx,medi,'-',lw=4,c='k',label=r'$\rm TNG:$ $\rm without$ $\rm resolved$ $\rm dust$')
		else: ax.plot(cx,medi,'-',lw=4,c='k')
		
		return sigma

#fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,30))
#row=3
#col=1
redshifts=[4,5,6,8]
snapnums=[21,17,13,8]
sigmas=np.zeros((5,len(cx)))

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(4):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	sigmas[i,:]=plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=27.0)
	if i in [0,1,2,3]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=4,frameon=False)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.10, 0.92, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_ylim(9.4,13.1)
	ax.set_xlim(-16.2,-23.5)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_xlabel(r'$M_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
	ax.set_ylabel(r'$\log{(M_{\rm halo}/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'UV-mhalo-'+str(snapnums[i])+'.pdf',fmt='pdf')
