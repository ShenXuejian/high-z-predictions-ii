import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager

from slopecorr_module import *
from magcorr_module import *
from magcorr_module_nmap import *
from mstarcorr_module import *

#bins=np.linspace(-24,-16, 24)
bins=np.linspace(-3,-0.5,20)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
	'13' :{'TNG50-1':[-16,-20.0],'TNG100-1':[-19.7,-21.5],'TNG300-1':[-21.2,-24]},
	'17' :{'TNG50-1':[-16,-20.5],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-21.2,-24]},
	'21' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-18.8,-22.0],'TNG300-1':[-21.2,-24]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter<=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter<=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter<=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def up(x):
	return np.percentile(x[np.isfinite(x)],84)

def lo(x):
	return np.percentile(x[np.isfinite(x)],16)

def plt_data(snapnum,redshift,model):
	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()

	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C

	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	halomass=mhalo['TNG100-1'][subids]
	if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
	else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
	f.close()

	fname=slopePath+'TNG100-1/sedslope_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	slope=f.root.sedslope[:].copy()
	f.close()

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        f.close()

	halomass=mhalo['TNG100-1'][subids]
	slope[:,0,3] += get_corr_slope(halomass,'TNG100-1',snapnum, (0,3))
	slope[:,1,3] += get_corr_slope(halomass,'TNG100-1',snapnum, (1,3))
	mstar = mstar * get_corr_mstar(halomass, 'TNG100-1', snapnum)

	bestfit=best_fits[model][str(snapnum)]+1
	Auv = mags[:,bestfit] - mags[:,0]
	valid = np.isfinite(Auv) & (mstar>np.power(10.,10.5)) & (mstar<np.power(10.,11.0))
	Auv[Auv<0]=0

	num100,b,_=st.binned_statistic(slope[valid,1,3], Auv[valid] , statistic='count', bins=bins)
	med100,b,_=st.binned_statistic(slope[valid,1,3], Auv[valid] , statistic='median', bins=bins)
	sigma100,b,_=st.binned_statistic(slope[valid,1,3], Auv[valid] , statistic=std, bins=bins)
	id=(num100>10)
	ax.errorbar(cx[id],med100[id],yerr=sigma100[id],linestyle='-',lw=4,marker='o',markersize=15,markeredgecolor='crimson',capthick=4,capsize=9,c='crimson')

	fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags= f.root.band_magnitudes[:,0,:].copy()
        halomass=mhalo['TNG300-1'][subids]
        if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG300-1',snapnum,model)
        else: mags=mags+get_corr(halomass,'TNG300-1',snapnum,model)
        f.close()

        fname=slopePath+'TNG300-1/sedslope_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        slope=f.root.sedslope[:].copy()
        f.close()

        fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        f.close()

        halomass=mhalo['TNG300-1'][subids]
        slope[:,0,3] += get_corr_slope(halomass,'TNG300-1',snapnum, (0,3))
        slope[:,1,3] += get_corr_slope(halomass,'TNG300-1',snapnum, (1,3))
        mstar = mstar * get_corr_mstar(halomass, 'TNG300-1', snapnum)

        bestfit=best_fits[model][str(snapnum)]+1
        Auv = mags[:,bestfit] - mags[:,0]
        valid = np.isfinite(Auv) & (mstar>np.power(10.,10.5)) & (mstar<np.power(10.,11.0))
        Auv[Auv<0]=0

        num300,b,_=st.binned_statistic(slope[valid,1,3], Auv[valid] , statistic='count', bins=bins)
        med300,b,_=st.binned_statistic(slope[valid,1,3], Auv[valid] , statistic='median', bins=bins)
        sigma300,b,_=st.binned_statistic(slope[valid,1,3], Auv[valid] , statistic=std, bins=bins)
        id=(num300>10)
        ax.errorbar(cx[id],med300[id],yerr=sigma300[id],linestyle='-',lw=4,marker='o',markersize=15,markeredgecolor='crimson',capthick=4,capsize=9,c='crimson')

	ax.plot([],[],linestyle='-',lw=4,c='crimson',label=r'$\log{M_{\ast}} \in [10.5,11.0]{\rm M}_{\odot}$')

	######################################
	#observations
	data = np.genfromtxt("obdata/IRX/Alvarez2015_z3.dat", names=True)
	id = data['bin']==2
	ax.errorbar( data['beta'][id], data['Auv'][id], yerr=(data['Auv'][id]-data['lo'][id], data['up'][id]-data['Auv'][id]), xerr=(data['beta'][id]-data['left'][id],data['right'][id]-data['beta'][id]), linestyle='none', marker='o', markersize=15, capsize=0, c='royalblue', mec='royalblue', label=r'$\rm Alvarez-Marquez+$ $\rm 2015$' )

	def convert(logIRX,B=1.71):
		return np.log10(10**logIRX/B+1)*2.5

	######################################
	betafit = np.linspace(-4,0,100)
	Auvfit  = betafit*1.99 + 4.43
	ax.plot(betafit,Auvfit,'-',c='k',lw=6,alpha=0.3,label=r'$\rm Meurer+$ $\rm 1999$')

	betafit = np.linspace(-4,0,100)
        Auvfit  = betafit*2.04 + 3.36
        ax.plot(betafit,Auvfit,'-',c='gray',lw=6,alpha=0.3,label=r'$\rm Casey+$ $\rm 2014$')

	betafit = np.linspace(-4,0,100)
	Auvfit  = betafit*1.1 + 2.45
	ax.plot(betafit,Auvfit,'-',c='navy',lw=6,alpha=0.3,label=r'$\rm SMC$')

	

#fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,30))
#row=3
#col=1
redshifts=[3]
snapnums=[25]

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(1):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=22)
	if i in [0]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=2,frameon=True)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.90, 0.08, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

	ax.set_xlim(-2.4,-0.2)
	ax.set_ylim(0.1,4.4)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_ylabel(r'$A_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
	ax.set_xlabel(r'$\beta$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'Auv-beta-detail2.pdf',fmt='pdf')

