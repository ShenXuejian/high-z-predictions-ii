import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager
from slopecorr_module import *
from magcorr_module import *
from magcorr_module_nmap import *
from sfrcorr_module import *
from mstarcorr_module import *

bins=np.linspace(-24,-16, 24)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
	'11' :{'TNG50-1':[-16,-20.0],'TNG100-1':[-19.8,-21.5],'TNG300-1':[-21.2,-24]},
	'13' :{'TNG50-1':[-16,-20.0],'TNG100-1':[-19.8,-21.5],'TNG300-1':[-21.2,-24]},
	'17' :{'TNG50-1':[-16,-20.5],'TNG100-1':[-19.8,-22.0],'TNG300-1':[-20.95,-24]},
	'21' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-19.6,-22.0],'TNG300-1':[-21.2,-24]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter<=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter<=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter<=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def plt_data(snapnum,redshift,model):
	ssfrcut = -9.0

	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()
	
	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C
	
	fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	f.close()

	fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	mstar=f.root.stellarmass[:].copy()
	subids=f.root.subids[:].copy()
	f.close()

        fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
	ssfr = np.log10(sfr/mstar)

	fname=slopePath+'TNG50-1/sedslope_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        slope=f.root.sedslope[:].copy()
	f.close()				

	bestfit=best_fits[model][str(snapnum)]+1	
	valid0 = np.isfinite(slope[:,1,0]) & (ssfr>ssfrcut)
	valid3 = np.isfinite(slope[:,1,3])# & (ssfr>-8.)
	# slope[:,1,3] is the slopes derived with an non-fiducial method, not used finally in plots
	
	num50_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic='count', bins=bins)
	med50_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic='median', bins=bins)
	sigma50_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic=std, bins=bins)

	num50_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic='count', bins=bins)
        med50_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic='median', bins=bins)
        sigma50_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic=std, bins=bins)		

	#id=(num50>10)
	#ax.plot(cx[id],med50[id],linestyle='-',lw=4 ,c='royalblue')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')
		
	num50i,b,_=st.binned_statistic(mags[valid0,0], slope[valid0,0,0] , statistic='count', bins=bins)
	med50i,b,_=st.binned_statistic(mags[valid0,0], slope[valid0,0,0] , statistic='median', bins=bins)
	#id=(num50i>10)
	#ax.plot(cx[id],med50i[id],':',lw=6,c='royalblue',alpha=0.6)
	
	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags= f.root.band_magnitudes[:,0,:].copy()
        halomass=mhalo['TNG100-1'][subids]
        if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
        else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
        f.close()

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()
        mstar=mstar * get_corr_mstar(halomass,'TNG100-1',snapnum)

        fname=sfrPath+'TNG100-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG100-1',snapnum)
        ssfr = np.log10(sfr/mstar)
		
	fname=slopePath+'TNG100-1/sedslope_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        slope=f.root.sedslope[:].copy()
        f.close()

	halomass=mhalo['TNG100-1'][subids]
	for i in range(slope.shape[2]):
                slope[:,0,i] += get_corr_slope(halomass,'TNG100-1',snapnum, (0,i))
                slope[:,1,i] += get_corr_slope(halomass,'TNG100-1',snapnum, (1,i))

	bestfit=best_fits[model][str(snapnum)]+1
        valid0 = np.isfinite(slope[:,1,0]) & (ssfr>ssfrcut)# & (slope[:,1] < 5) & (slope[:,1] > -5)
	valid3 = np.isfinite(slope[:,1,3])# & (ssfr>-8.)        

        num100_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic='count', bins=bins)
        med100_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic='median', bins=bins)
        sigma100_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic=std, bins=bins)
	num100_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic='count', bins=bins)
        med100_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic='median', bins=bins)
        sigma100_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic=std, bins=bins)

	#id=(num100>10)
	#ax.plot(cx[id],med100[id],linestyle='-',lw=4 ,c='crimson')
	#ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')

	num100i,b,_=st.binned_statistic(mags[valid0,0], slope[valid0,0,0] , statistic='count', bins=bins)
	med100i,b,_=st.binned_statistic(mags[valid0,0], slope[valid0,0,0] , statistic='median', bins=bins)
	#id=(num100i>10)
	#ax.plot(cx[id],med100i[id],':',lw=6,c='crimson',alpha=0.6)

	fname=the_path+"TNG300-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	halomass=mhalo['TNG300-1'][subids]
	if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG300-1',snapnum,model)
	else: mags=mags+get_corr(halomass,'TNG300-1',snapnum,model)
	f.close()	
	
	fname=mstarPath+'TNG300-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        subids=f.root.subids[:].copy()
        f.close()
        mstar=mstar * get_corr_mstar(halomass,'TNG300-1',snapnum)

        fname=sfrPath+'TNG300-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG300-1',snapnum)
        ssfr = np.log10(sfr/mstar)	
	
	fname=slopePath+'TNG300-1/sedslope_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	slope=f.root.sedslope[:].copy()
	f.close()

	halomass=mhalo['TNG300-1'][subids]
	for i in range(slope.shape[2]):
                slope[:,0,i] += get_corr_slope(halomass,'TNG100-1',snapnum, (0,i))
                slope[:,1,i] += get_corr_slope(halomass,'TNG100-1',snapnum, (1,i))

        bestfit=best_fits[model][str(snapnum)]+1
        valid0 = np.isfinite(slope[:,1,0]) & (ssfr>ssfrcut)# & (slope[:,1] < 5) & (slope[:,1] > -5)
	valid3 = np.isfinite(slope[:,1,3]) # & (ssfr>-9.5)	
        
        num300_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic='count', bins=bins)
        med300_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic='median', bins=bins)
        sigma300_0,b,_=st.binned_statistic(mags[valid0,bestfit], slope[valid0,1,0] , statistic=std, bins=bins)
	num300_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic='count', bins=bins)
        med300_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic='median', bins=bins)
        sigma300_3,b,_=st.binned_statistic(mags[valid3,bestfit], slope[valid3,1,3] , statistic=std, bins=bins)


        #id=(num300>10)
        #ax.plot(cx[id],med300[id],linestyle='-',lw=4 ,c='seagreen')
        #ax.errorbar(cx[id],med50[id],yerr=sigma50[id],linestyle='-',lw=4,marker='s',markersize=10,markeredgewidth=4,markeredgecolor='royalblue',capsize=10,c='royalblue')


        num300i,b,_=st.binned_statistic(mags[valid0,0], slope[valid0,0,0] , statistic='count', bins=bins)
        med300i,b,_=st.binned_statistic(mags[valid0,0], slope[valid0,0,0] , statistic='median', bins=bins)
        #id=(num300i>10)
        #ax.plot(cx[id],med300i[id],':',lw=6,c='seagreen',alpha=0.6)

	med_combined_0  = combine(med50_0,med100_0,med300_0,num50_0,num100_0,num300_0,snapnum)
	sig_combined_0  = combine(sigma50_0,sigma100_0,sigma300_0,num50_0,num100_0,num300_0,snapnum)
	med_combined_3  = combine(med50_3,med100_3,med300_3,num50_3,num100_3,num300_3,snapnum)
        sig_combined_3  = combine(sigma50_3,sigma100_3,sigma300_3,num50_3,num100_3,num300_3,snapnum)

	if snapnum==21:
		ax.errorbar(cx,med_combined_0,yerr=sig_combined_0,linestyle='-',c='royalblue',mec='royalblue',marker='o',ms=15,capsize=9,capthick=4,label=r'$\rm TNG:$ $\rm with$ $\rm all$ $\rm dust$')
		#ax.errorbar(cx,med_combined_3,yerr=sig_combined_3,linestyle='-',c='dimgray',mec='dimgray',marker='o',ms=15,capsize=9,capthick=4,label=r'$\rm phot$ $\rm 1450-3000$')
	else:
		ax.errorbar(cx,med_combined_0,yerr=sig_combined_0,linestyle='-',c='royalblue',mec='royalblue',marker='o',ms=15,capsize=9,capthick=4)
		#ax.errorbar(cx,med_combined_3,yerr=sig_combined_3,linestyle='-',c='dimgray',mec='dimgray',marker='o',ms=15,capsize=9,capthick=4)

	medi_combined = combine(med50i,med100i,med300i,num50i,num100i,num300i,snapnum)
	if snapnum==21:
		ax.plot(cx,medi_combined,'-',c='k',label=r'$\rm TNG:$ $\rm without$ $\rm resolved$ $\rm dust$')
	else: ax.plot(cx,medi_combined,'-',c='k')

	##############################################
	if snapnum in [21,17,13,11,8]:
			data=np.genfromtxt("obdata/bouwens2014.dat",names=True)
			id=data['z']==redshift
			if snapnum==21: ax.errorbar(data['muv'][id],data['beta'][id],yerr=np.sqrt(data['err_rand'][id]**2+data['err_sys'][id]**2),linestyle='none',c='crimson',mec='crimson',marker='o',ms=15,capsize=0,label=r'$\rm Bouwens+$ $\rm 2014$')
                        else: ax.errorbar(data['muv'][id],data['beta'][id],yerr=np.sqrt(data['err_rand'][id]**2+data['err_sys'][id]**2),linestyle='none',c='crimson',mec='crimson',marker='o',ms=15,capsize=0)

	if snapnum in [21,17,13,11]:
                        data=np.genfromtxt("obdata/bouwens2012.dat",names=True)
                        id=data['z']==redshift
                        if snapnum==21: ax.errorbar(data['muv'][id],data['beta'][id],yerr=np.sqrt(data['err_rand'][id]**2+data['err_sys'][id]**2),linestyle='none',c='chocolate',mec='chocolate',marker='o',ms=15,capsize=0,label=r'$\rm Bouwens+$ $\rm 2012$')
                        else: ax.errorbar(data['muv'][id],data['beta'][id],yerr=np.sqrt(data['err_rand'][id]**2+data['err_sys'][id]**2),linestyle='none',c='chocolate',mec='chocolate',marker='o',ms=15,capsize=0)
		
	if snapnum in [17,13,11]:
                        data=np.genfromtxt("obdata/dunlop2012.dat",names=True)
                        id=data['z']==redshift
                        if snapnum==17: ax.errorbar(data['muv'][id],data['beta'][id],yerr=data['err'][id],linestyle='none',c='seagreen',mec='seagreen',marker='o',ms=15,capsize=0,label=r'$\rm Dunlop+$ $\rm 2013$')
			else: ax.errorbar(data['muv'][id],data['beta'][id],yerr=data['err'][id],linestyle='none',c='seagreen',mec='seagreen',marker='o',ms=15,capsize=0)

	if snapnum in [21,17,13,11,8]:
                        data=np.genfromtxt("obdata/finkelstein12.dat",names=True)
                        id=data['z']==redshift
                        if snapnum==21: ax.errorbar(data['muv'][id],data['beta'][id],yerr=(data['beta'][id]-data['beta_lo'][id],data['beta_up'][id]-data['beta'][id]),linestyle='none',c='darkorchid',mec='darkorchid',marker='o',ms=15,capsize=0,label=r'$\rm Finkelstein+$ $\rm 2012$')
			else: ax.errorbar(data['muv'][id],data['beta'][id],yerr=(data['beta'][id]-data['beta_lo'][id],data['beta_up'][id]-data['beta'][id]),linestyle='none',c='darkorchid',mec='darkorchid',marker='o',ms=15,capsize=0)

#fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,30))
#row=3
#col=1
redshifts=[4,5,6]
snapnums=[21,17,13]

#x0,y0,width,height,wspace,hspace=0.11,0.04,0.79,0.31,0.08,0
for i in range(3):
	#ax=fig.add_axes([x0,y0+(2-i)*height+(2-i)*hspace,width,height])
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i],"C")
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=27)
	if i in [0,1]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=4,frameon=False)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.23, 0.84, r'$\log{({\rm sSFR}/{\rm yr}^{-1})}>-9.0$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')

	ax.text(0.10, 0.92, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_ylim(-2.55,-1.55)
	ax.set_xlim(-16.2,-23.5)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_xlabel(r'$M_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
	ax.set_ylabel(r'$\beta$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'UV-beta-'+str(i)+'.pdf',fmt='pdf')

