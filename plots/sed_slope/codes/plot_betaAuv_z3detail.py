import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tables
from scipy import stats as st
import sys
SIM = ' '
from data import *
import matplotlib.font_manager

from slopecorr_module import *
from magcorr_module import *
from magcorr_module_nmap import *
from mstarcorr_module import *
from sfrcorr_module import *

#bins=np.linspace(-24,-16, 24)
bins=np.linspace(-3,-0.5,20)
cx= (bins[:-1]+bins[1:])/2.

matplotlib.style.use('classic')

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

eff={
	'13' :{'TNG50-1':[-16,-20.0],'TNG100-1':[-19.7,-21.5],'TNG300-1':[-21.2,-24]},
	'17' :{'TNG50-1':[-16,-20.5],'TNG100-1':[-19.0,-22.0],'TNG300-1':[-21.2,-24]},
	'21' :{'TNG50-1':[-16,-21.0],'TNG100-1':[-18.8,-22.0],'TNG300-1':[-21.2,-24]}
}

def combine(v50,v100,v300,n50,n100,n300,snapnum):
	v50[np.invert(np.isfinite(v50))]=0
	n50[np.invert(np.isfinite(v50))]=1e-37
	v100[np.invert(np.isfinite(v100))]=0
	n100[np.invert(np.isfinite(v100))]=1e-37
	v300[np.invert(np.isfinite(v300))]=0
	n300[np.invert(np.isfinite(v300))]=1e-37

        def f(x,n):
                return x*n**2
        normalization=0.0*cx
        combined=0.0*cx
	binscenter=cx
        id50 = (binscenter<=eff[str(snapnum)]["TNG50-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG50-1"][1]) 
        id100= (binscenter<=eff[str(snapnum)]["TNG100-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG100-1"][1])
        id300= (binscenter<=eff[str(snapnum)]["TNG300-1"][0]) & (binscenter>=eff[str(snapnum)]["TNG300-1"][1]) & (n300>10)

        combined[id50] += f(v50[id50],n50[id50])
        normalization[id50] += n50[id50]**2
        combined[id100] += f(v100[id100],n100[id100])
        normalization[id100] += n100[id100]**2
        combined[id300] += f(v300[id300],n300[id300])
        normalization[id300] += n300[id300]**2
        return combined/normalization

def std(x):
	return (np.percentile(x[np.isfinite(x)],84)-np.percentile(x[np.isfinite(x)],16))/2.
	#return np.std(x)

def up(x):
	return np.percentile(x[np.isfinite(x)],84)

def lo(x):
	return np.percentile(x[np.isfinite(x)],16)

def plt_data(snapnum,redshift,ssfrcut = -9.0,obs=True):
	model = 'C'	

	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
        f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
        mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
        mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
        mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
        f.close()

	if model=='B': the_path=outpath_B
	if model=='C': 
		if snapnum<=13: 
			the_path=outpath_C_nmap
		else: 
			the_path=outpath_C

	fname=the_path+"TNG50-1/output/magnitudes_"+str(snapnum)+".hdf5"
        f=tables.open_file(fname)
        subids=f.root.subids[:].copy()
        mags= f.root.band_magnitudes[:,0,:].copy()
        f.close()

        fname=slopePath+'TNG50-1/sedslope_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        slope=f.root.sedslope[:].copy()
        f.close()

        fname=mstarPath+'TNG50-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        f.close()

	fname=sfrPath+'TNG50-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        ssfr = np.log10(sfr/mstar)

        halomass = mhalo['TNG50-1'][subids]
        bestfit=best_fits[model][str(snapnum)]+1
        Auv = mags[:,bestfit] - mags[:,0]

        valid = np.isfinite(Auv) & (mstar>np.power(10.,9.5)) & (mstar<np.power(10.,10.5)) & (ssfr>ssfrcut) & (mags[:,bestfit]<-17)
        Auv[Auv<0]=0
        num50,b,_=st.binned_statistic(slope[valid,1,0], Auv[valid] , statistic='count', bins=bins)
        med50,b,_=st.binned_statistic(slope[valid,1,0], Auv[valid] , statistic='median', bins=bins)
        sigma50,b,_=st.binned_statistic(slope[valid,1,0], Auv[valid] , statistic=std, bins=bins)
	id=(num50>10)
        if ssfrcut == -9.5:
		ax.plot(cx[id],med50[id],linestyle=':',lw=4,marker=None,c='crimson')
	else: ax.plot(cx[id],med50[id],linestyle=':',lw=4,marker=None,c='royalblue')	

	fname=the_path+"TNG100-1/output/magnitudes_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname)
	subids=f.root.subids[:].copy()
	mags= f.root.band_magnitudes[:,0,:].copy()
	halomass=mhalo['TNG100-1'][subids]
	if snapnum<=13: mags=mags+get_corr_nmap(halomass,'TNG100-1',snapnum,model)
	else: mags=mags+get_corr(halomass,'TNG100-1',snapnum,model)
	f.close()

	fname=slopePath+'TNG100-1/sedslope_'+str(snapnum)+'.hdf5'
	f=tables.open_file(fname)
	slope=f.root.sedslope[:].copy()
	f.close()

	fname=mstarPath+'TNG100-1/stellarmass_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        mstar=f.root.stellarmass[:].copy()
        f.close()

	halomass=mhalo['TNG100-1'][subids]
	slope[:,0,0] += get_corr_slope(halomass,'TNG100-1',snapnum, (0,0))
	slope[:,1,0] += get_corr_slope(halomass,'TNG100-1',snapnum, (1,0))
	mstar = mstar * get_corr_mstar(halomass, 'TNG100-1', snapnum)

	fname=sfrPath+'TNG100-1/starformationrate_'+str(snapnum)+'.hdf5'
        f=tables.open_file(fname)
        sfr=f.root.starformationrate[:].copy()
        subids_all=f.root.subids[:].copy()
        f.close()
        sfr=sfr*get_corr_sfr(halomass,'TNG100-1',snapnum)
        ssfr = np.log10(sfr/mstar)

	bestfit=best_fits[model][str(snapnum)]+1
	Auv = mags[:,bestfit] - mags[:,0]
	valid = np.isfinite(Auv) & (mstar>np.power(10.,9.5)) & (mstar<np.power(10.,10.5)) & (ssfr>ssfrcut) & (mags[:,bestfit]<-17)
	Auv[Auv<0]=0

	num100,b,_=st.binned_statistic(slope[valid,1,0], Auv[valid] , statistic='count', bins=bins)
	med100,b,_=st.binned_statistic(slope[valid,1,0], Auv[valid] , statistic='median', bins=bins)
	sigma100,b,_=st.binned_statistic(slope[valid,1,0], Auv[valid] , statistic=std, bins=bins)
	id=(num100>10)
	if ssfrcut == -9.5:
		ax.errorbar(cx[id],med100[id],yerr=sigma100[id],linestyle='-',lw=4,marker='o',markersize=15,markeredgecolor='crimson',capthick=4,capsize=9,c='crimson')
	else: ax.errorbar(cx[id],med100[id],yerr=sigma100[id],linestyle='-',lw=4,marker='o',markersize=15,markeredgecolor='royalblue',capthick=4,capsize=9,c='royalblue')

	if ssfrcut == -9.5: ax.plot([],[],linestyle='-',lw=4,c='crimson',label=r'$\log{({\rm sSFR}/{\rm yr}^{-1})}>-9.5$')
	else: ax.plot([],[],linestyle='-',lw=4,c='royalblue',label=r'$\log{({\rm sSFR}/{\rm yr}^{-1})}>-9.0$')

	######################################
	if obs==True:
		#observations
		data = np.genfromtxt("obdata/IRX/Alvarez2015_z3.dat", names=True)
		id = data['bin']==1
		ax.errorbar( data['beta'][id], data['Auv'][id], yerr=(data['Auv'][id]-data['lo'][id], data['up'][id]-data['Auv'][id]), xerr=(data['beta'][id]-data['left'][id],data['right'][id]-data['beta'][id]), linestyle='none', marker='o', markersize=15, capsize=0, c='chocolate', mec='chocolate', label=r'$\rm \'{A}lvarez-M\'{a}rquez+$ $\rm 2016$' )

		def convert(logIRX,B=1.71):
			return np.log10(10**logIRX/B+1)*2.5

		data = np.genfromtxt("obdata/IRX/Alvarez2019_z3.dat", names=True)
		ax.errorbar(data["beta"],convert(data["logIRX"],B=1.71),yerr=(convert(data["logIRX"],B=1.71)-convert(data["lo"],B=1.71),convert(data["up"],B=1.71)-convert(data["logIRX"],B=1.71)),xerr=(data["beta"]-data["left"],data["right"]-data["beta"]), color='deeppink', mec='deeppink', capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm \'{A}lvarez-M\'{a}rquez+$ $\rm 2019$')	

		data=np.genfromtxt("obdata/IRX/koprowski2018_z3.dat",names=True)
		select=data["type"]==1
		ax.errorbar(data["beta"][select],convert(data["logIRX"][select],B=1.56),yerr=(convert(data["logIRX"][select],B=1.56)-convert(data["lo"][select],B=1.56),convert(data["up"][select],B=1.56)-convert(data["logIRX"][select],B=1.56)),xerr=(data["beta"][select]-data["left"][select],data["right"][select]-data["beta"][select]), color='seagreen', mec='seagreen', capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Koprowski+$ $\rm 2018$ ($\rm z=3.35$)')
		select=data["type"]==0
		ax.errorbar(data["beta"][select],convert(data["logIRX"][select],B=1.56),xerr=(data["beta"][select]-data["left"][select],data["right"][select]-data["beta"][select]), color='seagreen', mec='seagreen', capsize=0, marker='o',markersize=15, linestyle='none')	
		ax.errorbar(data["beta"][select],convert(data["logIRX"][select],B=1.56),yerr=0.3,uplims=True, color='seagreen', mec='seagreen', capsize=9, marker='o',markersize=15, linestyle='none')

		data=np.genfromtxt("obdata/IRX/Mclure2018_z2_5.dat",names=True)
	        select=data["type"]==1
	        ax.errorbar(data["beta"][select],convert(data["logIRX"][select]),yerr=(convert(data["logIRX"][select])-convert(data["lo"][select]),convert(data["up"][select])-convert(data["logIRX"][select])),xerr=(data["beta"][select]-data["left"][select],data["right"][select]-data["beta"][select]), color='darkorchid', mec='darkorchid', capsize=0, marker='o',markersize=15, linestyle='none',label=r'$\rm Mclure+$ $\rm 2018$ ($\rm z=2.5$)')
	        select=data["type"]==0
	        ax.errorbar(data["beta"][select],convert(data["logIRX"][select]),xerr=(data["beta"][select]-data["left"][select],data["right"][select]-data["beta"][select]), color='darkorchid', mec='darkorchid', capsize=0, marker='o',markersize=15, linestyle='none')
	        ax.errorbar(data["beta"][select],convert(data["logIRX"][select]),yerr=0.3,uplims=True, color='darkorchid', mec='darkorchid', capsize=9, marker='o',markersize=15, linestyle='none')

		######################################
		betafit = np.linspace(-4,0,100)
		Auvfit  = betafit*1.99 + 4.43
		ax.plot(betafit,Auvfit,'-',c='k',lw=6,alpha=0.3,label=r'$\rm Meurer+$ $\rm 1999$ ($\rm z\sim 0$)')
	
		betafit = np.linspace(-4,0,100)
	        Auvfit  = betafit*2.04 + 3.36
	        ax.plot(betafit,Auvfit,'-',c='gray',lw=6,alpha=0.3,label=r'$\rm Casey+$ $\rm 2014$ ($\rm z\sim 0$)')
	
		betafit = np.linspace(-4,0,100)
		Auvfit  = betafit*1.1 + 2.45
		ax.plot(betafit,Auvfit,'-',c='navy',lw=6,alpha=0.3,label=r'$\rm SMC$ ($\rm z\sim 0$)')

redshifts=[3]
snapnums=[25]

for i in range(1):
	fig=plt.figure(figsize = (15,10))
	ax=fig.add_axes([0.11,0.12,0.79,0.83])

	plt_data(snapnums[i],redshifts[i],ssfrcut = -9.5, obs=False)
	plt_data(snapnums[i],redshifts[i])
	#ax.axhspan(-8,np.log10( 5./lenbin/(boxlength/1000./hubble)**3 ),color='grey',alpha=0.1)

	prop = matplotlib.font_manager.FontProperties(size=21)
	if i in [0]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=2,frameon=False)
	#if i in [2]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=2,loc=1,frameon=False)

	ax.text(0.73, 0.16, r'$\log{({\rm sSFR}/{\rm yr}^{-1})}>-9.0/-9.5$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')
        ax.text(0.87, 0.23, r'$M_{\rm UV}<-17$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')
	ax.text(0.77, 0.30, r'$\log{(M_{\ast}/{\rm M}_{\odot})} \in [9.5,10.5]$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='dimgray')

	ax.text(0.90, 0.08, r'${\rm z='+str(redshifts[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)

	ax.set_xlim(-2.4,-0.2)
	ax.set_ylim(0.1,4.4)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	ax.set_ylabel(r'$A_{\rm UV} {\rm [mag]}$',fontsize=40,labelpad=2.5)
	ax.set_xlabel(r'$\beta$',fontsize=40,labelpad=2.5)
	#plt.show()
	plt.savefig(figpath+'Auv-beta-detail.pdf',fmt='pdf')

