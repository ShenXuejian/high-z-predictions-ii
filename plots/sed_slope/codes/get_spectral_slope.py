import numpy as np
from scipy.optimize import curve_fit
import astropy.constants as con
import sys
import os
import tables
SIM=sys.argv[1]
from data import *
snapnum=int(sys.argv[2])
from mpi4py import MPI

#outpath='/n/mvogelsfs01/sxj/DUST_MODEL/modelC/TUNE_backup/'
#outpath=outpath_C_ndust
if snapnum>13: outpath=outpath_C
else: outpath=outpath_C_nmap

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def get_slope(fname,startloc,endloc):	
        data=np.genfromtxt(fname,names=("lamb","fnu"))
        lamb=data['lamb']*1e4 #unit A
        flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
        #lum=flux * 1e-23 * 4.* np.pi * (10.*con.pc.value*100)**2 #unit erg/s/Hz

	index = np.arange(0,len(lamb),dtype=np.int32)
	startid = index[(lamb-startloc)>0][0]
	endid   = index[(lamb-endloc)<0][-1]

	UVlamb = lamb[startid:endid+1]
	flamb = flux[startid:endid+1]*con.c.value/lamb[startid:endid+1]**2 

	if len(flamb[np.invert(np.isfinite(flamb))]) != 0: return np.nan
	if len(flamb[flamb<=0]) != 0: return np.nan

	def flin(x,beta,A):
		return beta*(x-np.log10(1600.))+A

	def fpower(x,beta,A):
		return A*np.power(x/1600.,beta)

	best_fit,_ = curve_fit(flin, np.log10(UVlamb), np.log10(flamb), p0=(-1,np.log10(flamb[0])) )
	#best_fit,_ = curve_fit(fpower, UVlamb,flamb, p0=(-1,flamb[0])) 
	#print best_fit
	#print (flin(np.log10(UVlamb),best_fit[0],best_fit[1])-np.log10(flamb))/np.log10(flamb)
	return best_fit[0]

def get_slope_2(fname,startloc,endloc):
	data=np.genfromtxt(fname,names=("lamb","fnu"))
        lamb=data['lamb']*1e4 #unit A
        flux=data['fnu']*(1*1000*1000/10.)**2 #unit Jy
        #lum=flux * 1e-23 * 4.* np.pi * (10.*con.pc.value*100)**2 #unit erg/s/Hz

        index = np.arange(0,len(lamb),dtype=np.int32)
        startids = index[ np.abs(lamb-startloc)<50 ]
        endids   = index[ np.abs(lamb-endloc)<50 ]

        f1 = np.mean(flux[startids])*con.c.value/startloc**2
	f2 = np.mean(flux[endids])*con.c.value/endloc**2

	slope = (np.log10(f1)-np.log10(f2))/(np.log10(startloc)-np.log10(endloc))
	return slope

comm.Barrier()

ids=np.genfromtxt(outpath+SIM+"/snap_"+str(snapnum)+"/subids")

if len(ids.shape)!=0:
	Ntotsubs               = ids.shape[0]
	ids_in_subgroup        = np.arange(0, Ntotsubs, dtype='uint32')

	betas=np.zeros((len(ids),2,6))
	betas_global=np.zeros((len(ids),2,6))

	index                  = (ids % size) == rank
	dosubs                 = ids[index]
	Ndosubs                = dosubs.shape[0]
	doid_in_subgroup       = ids_in_subgroup[index]

	for si in range(0, Ndosubs):	
		print rank, si, Ndosubs, Ntotsubs
        	sys.stdout.flush()
		subnum=dosubs[si]
	        relaid=doid_in_subgroup[si]

		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/nodust_neb_sed.dat'
		if os.path.isfile(fname)==True: 
			betas[relaid,0,0] = get_slope(fname,1450.,3000.)
			betas[relaid,0,1] = get_slope(fname,1450.,2500.)
			betas[relaid,0,2] = get_slope(fname,1450.,2000.)
			betas[relaid,0,3] = get_slope_2(fname,1450.,3000.)
			betas[relaid,0,4] = get_slope_2(fname,1450.,2500.)
			betas[relaid,0,5] = get_slope_2(fname,1450.,2000.)
			#print betas[relaid,0,:]
			#exit()
		fname=outpath+SIM+"/snap_"+str(snapnum)+"/output"+str(int(subnum))+'/dusty0_neb_sed.dat'
                if os.path.isfile(fname)==True: 
			betas[relaid,1,0] = get_slope(fname,1450.,3000.)
			betas[relaid,1,1] = get_slope(fname,1450.,2500.)
			betas[relaid,1,2] = get_slope(fname,1450.,2000.)
			betas[relaid,1,3] = get_slope_2(fname,1450.,3000.)
			betas[relaid,1,4] = get_slope_2(fname,1450.,2500.)
			betas[relaid,1,5] = get_slope_2(fname,1450.,2000.)
			#print betas[relaid,1,:]
                        #exit()
	comm.Barrier()
	comm.Allreduce(betas,        betas_global,     op=MPI.SUM)

if rank==0:
	fname="./outpath/"+SIM+"/sedslope_"+str(snapnum)+".hdf5"
	f=tables.open_file(fname, mode = "w")
	f.create_array(f.root, "sedslope", betas_global)

	if len(ids.shape)==0: f.create_array(f.root, "subids", np.array([ids.astype(np.uint32)]))
	else: f.create_array(f.root, "subids", ids.astype(np.uint32))
	f.close()

