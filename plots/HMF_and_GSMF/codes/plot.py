import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import tables
import sys
SIM = 'TNG300-1'
from data import *
import matplotlib.font_manager
from scipy.optimize import curve_fit
import illustris_python as il
matplotlib.style.use('classic')

smass_bins=np.linspace(7,12.5,23)
cx=(smass_bins[1:]+smass_bins[:-1])/2.
lenbin=cx[5]-cx[4]

matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

def mstar_sch(M,phi_s,M_s,alpha): #M in log10; phi_s in log10
        return np.log10( np.log(10.)*(10**phi_s)*np.power(10.,(M-M_s)*(alpha+1.))*np.exp(-np.power(10.,M-M_s)) )

def plt_data(snapnum,redshift,label=False):
	#cxfit=np.linspace(6.5,13,1000)
	#fits=np.genfromtxt("schechter_fits.dat",names=True)
	#id=fits["snap"]==snapnum
        #logpfit=mstar_sch(cxfit,fits["LogPhis"][id],fits["LogMs"][id],fits["alpha"][id])
        #if label==True: ax.plot(cxfit,logpfit,'-',lw=7,c='cyan',alpha=0.5,label=r'$\rm Schechter$ $\rm fit$')
        #else: ax.plot(cxfit,logpfit,'-',lw=7,c='cyan',alpha=0.5)

	mhalo={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
	f=tables.open_file("./simdata/data_"+str(snapnum)+".hdf5")
	mhalo["TNG50-1"]=f.root.TNG50.msub1[:].copy()
	mhalo["TNG100-1"]=f.root.TNG100.msub1[:].copy()
	mhalo["TNG300-1"]=f.root.TNG300.msub1[:].copy()
	f.close()

	#mhalo_DM={"TNG50-1":0,"TNG100-1":0,"TNG300-1":0}
	#mhalo_DM["TNG50-1"] =il.groupcat.loadSubhalos(base50_1_DM,snapnum,fields=["SubhaloMass"])*1e10/hubble
	#mhalo_DM["TNG100-1"]=il.groupcat.loadSubhalos(base100_1_DM,snapnum,fields=["SubhaloMass"])*1e10/hubble
	#mhalo_DM["TNG300-1"]=il.groupcat.loadSubhalos(base300_1_DM,snapnum,fields=["SubhaloMass"])*1e10/hubble

	boxlength = {"TNG50-1":35.,"TNG100-1":75.,"TNG300-1":205.}
	for sim in ["TNG50-1","TNG100-1","TNG300-1"]:
		valid_masses=mhalo[sim][np.isfinite(mhalo[sim])]
		count,b=np.histogram(np.log10(valid_masses*OmegaBaryon/Omega0),bins=smass_bins)
        	phi= np.log10(count/lenbin/((boxlength[sim]/hubble)**3))		
		ax.plot(cx,phi,c='royalblue')

		#valid_masses=mhalo_DM[sim][np.isfinite(mhalo_DM[sim])]
                #count,b=np.histogram(np.log10(valid_masses*OmegaBaryon/Omega0),bins=smass_bins)
                #phi= np.log10(count/lenbin/((boxlength[sim]/hubble)**3))
                #ax.plot(cx,phi,'--',dashes=(25,15),c='crimson')

	data=np.genfromtxt("../../GSMF/codes/combined/combined_SMF_"+str(snapnum)+".dat")
	phi_combined = data[:,1]
	print phi_combined
	if label==True:
		ax.plot(data[:,0],phi_combined,'seagreen',label=r'$\rm stellar$ $\rm mass$')
	else: ax.plot(data[:,0],phi_combined,'seagreen')

fig=plt.figure(figsize = (36,24))
#fig=plt.figure(figsize = (15,10))
row=3
col=3

x0,y0,width,height,wspace,hspace=0.05,0.05,0.3,0.3,0,0
for i in range(9):
	nh,nw=2-i/3,i%3
        ax=fig.add_axes([x0+nw*(width+wspace),y0+nh*(height+hspace),width,height])

	if i==0:
		plt_data(Snaps[i],Zs[i],label=True)
	else: 
		plt_data(Snaps[i],Zs[i])

	prop = matplotlib.font_manager.FontProperties(size=30)
	if Snaps[i] in [21]: ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)
	elif i==0: 	
		ax.legend(prop=prop,numpoints=1, borderaxespad=0.5,ncol=1,loc=3,frameon=True)

	ax.text(0.85, 0.92, r'${\rm z='+str(Zs[i])+r'}$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=40)
	ax.set_xlim(6.75,12.75)
	ax.set_ylim(-6.3,0.75)
	ax.axis('on')
	ax.tick_params(labelsize=30)
	ax.tick_params(axis='x', pad=7.5)
	ax.tick_params(axis='y', pad=2.5)
	ax.minorticks_on()
	if (i/row==col-1):
		ax.set_xlabel(r'$\log{(M/{\rm M}_{\odot})}$',fontsize=40,labelpad=2.5)
	else: ax.set_xticklabels([])
	if i%row==0:
		ax.set_ylabel(r'$\log(\phi[{\rm Mpc}^{-3}{\rm dex}^{-1}])$',fontsize=40,labelpad=2.5)
	else:
		ax.set_yticklabels([])
#plt.show()
plt.savefig(figpath+'HMF_VS_GSMF.pdf',fmt='pdf')
