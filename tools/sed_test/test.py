import numpy as np
from scipy.integrate import romberg
from scipy.interpolate import interp1d
import astropy.constants as con
import sys
import os
import tables

logP = sys.argv[1]
logC = sys.argv[2].zfill(2)
#Z = sys.argv[2].zfill(3)

line_info={
	"Halpha"  :np.array([6563., 6463., 6663., 6163., 6963.]),
	"Hbeta"   :np.array([4861., 4801., 4900., 4561., 4921.]),
	#"OII"     :np.array([3727., 3627., 3827., 3327., 4127.]),
	"OII"     :np.array([3727., 3677., 3757., 3427., 3757.]),
	"OIII"    :np.array([5000., 4921., 5100., 4900., 5300.]),
	"OIII5008":np.array([5008., 4980., 5100., 4980., 5500.])
}

def get_Lline(fname,lineloc):	
	data=np.genfromtxt(fname,names=("lamb","f1","f2"))
        lamb=data['lamb'] #unit m
        flux=data['f1'] 
        lum=flux 

        lamb=np.append(np.append(1e-20,lamb),1e10)
        lum=np.append(np.append(0,lum),0)

        frequency = con.c.value/lamb #unit Hz
        spec = lum
        f=interp1d(frequency, spec)
        BB=romberg(f , 3e8/lineloc[4]*1e10  , 3e8/lineloc[3]*1e10 ,  divmax=100)
        NB=romberg(f , 3e8/lineloc[2]*1e10  , 3e8/lineloc[1]*1e10 ,  divmax=100)
        wBB=lineloc[4]-lineloc[3]
        wNB=lineloc[2]-lineloc[1]
        fBB=BB/wBB
        fNB=NB/wNB

        if (fBB==0) or (fNB==0):
                EW=0
                Lline=0
        else:
                EW= wNB* (fNB-fBB)/(fBB-fNB*wNB/wBB)
                Lline= wNB* (fNB-fBB)/(1-wNB/wBB)
	if (Lline>0) and (EW>0):
		return np.log10(Lline), np.log10(EW)
	else: return -np.inf, 0
'''
#print "pressure,      OIII/Hbeta,     Halpha/Hbeta,     OII"
print "pressure,      OIII,       Halpha,       Hbeta,        OII"
plist = [4,5,6,7,8]
for logP in plist:
	logP = str(logP)
	fname = "./sedlib/Mappings_Z"+Z+"_C"+logC+"_p"+logP+".dat"
	LOII, _ = get_Lline(fname,line_info['OII'])
	LOIII,_ = get_Lline(fname,line_info['OIII'])
	LHbeta,_ = get_Lline(fname,line_info['Hbeta'])
	LHalpha,_= get_Lline(fname,line_info['Halpha'])
	print logP, LOIII, LHalpha, LHbeta, LOII, LOIII-LHalpha
'''

zlist = [5,20,40,100,200]
for Z in zlist:
        Z = str(Z).zfill(3)
        fname = "./sedlib/Mappings_Z"+Z+"_C"+logC+"_p"+logP+".dat"
        LOIII,_ = get_Lline(fname,line_info['OIII'])
        LHalpha,_= get_Lline(fname,line_info['Halpha'])
        print Z, LOIII-LHalpha




