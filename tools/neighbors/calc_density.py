import tables
import sys
import numpy as np
import readsnapHDF5 as rs
import readsubfHDF5
from astropy.cosmology import FlatLambdaCDM
import astropy.constants as con
from scipy.spatial import KDTree
SIM=sys.argv[1]
from data import *

snapnum=int(sys.argv[2])

cosmo        = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

def r_nearest(x,y,z,N):
        data=np.transpose(np.array([x,y,z]))
        tree=KDTree(data)
        result,_=tree.query(data,k=N+1)
        h=result[:,-1]
        return h

def r_aperture(x,y,z,R):
	data=np.transpose(np.array([x,y,z]))
        tree=KDTree(data)
        result=tree.query_ball_point(data,r=R)
	number=map(lambda x: len(x), result)
	return np.array(number)

redshift  = rs.snapshot_header(base + "/snapdir_"+str(snapnum).zfill(3)+"/snap_"+str(snapnum).zfill(3)).redshift
cat       = readsubfHDF5.subfind_catalog(base, snapnum, keysel=["SubhaloLenType","SubhaloPos","SubhaloHalfmassRadType","SubhaloMassInRadType"])

submass=cat.SubhaloMassInRadType[:,4]*1e10/hubble
masscut=100*mgas/hubble
index_selection        = (cat.SubhaloLenType[:,4] > 0) & (submass > masscut)
totsubs                = np.arange(0, cat.nsubs, dtype='uint32')[index_selection]

pos = cat.SubhaloPos[index_selection,:]/hubble   #comoving units

rho2Mpc = r_aperture(pos[:,0],pos[:,1],pos[:,2],2e3/hubble)/(4*np.pi/3)/(2e3/hubble)**3
rho5Mpc = r_aperture(pos[:,0],pos[:,1],pos[:,2],5e3/hubble)/(4*np.pi/3)/(5e3/hubble)**3

r5th= r_nearest(pos[:,0],pos[:,1],pos[:,2],5)
r4th= r_nearest(pos[:,0],pos[:,1],pos[:,2],4)
rho5th = (5+1)/(4*np.pi/3)/r5th**3
rho4th = (4+1)/(4*np.pi/3)/r4th**3
rho_45 = (rho4th+rho5th)/2.

rho_global = pos.shape[0]/(boxlength/hubble)**3
overdensity_45= (rho_45-rho_global)/rho_global
overdensity_2Mpc= (rho2Mpc-rho_global)/rho_global
overdensity_5Mpc= (rho5Mpc-rho_global)/rho_global

overdensity = np.zeros((pos.shape[0],3))
overdensity[:,0]=overdensity_45
overdensity[:,1]=overdensity_2Mpc
overdensity[:,2]=overdensity_5Mpc

fname='./output/overdensity_'+SIM+'_'+str(snapnum)+".hdf5"
f=tables.open_file(fname, mode = "w")
f.create_array(f.root, "overdensity", overdensity)
f.create_array(f.root, "subids", totsubs)
f.close()

